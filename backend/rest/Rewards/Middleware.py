from .Helper import Helper, ChangeType


class RewardMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        # check for new reward
        if (request.method == "POST" or request.method == "PUT") and str(request.path).find("challenge") > -1 and not request.user.is_anonymous:
            helper = Helper()
            if helper.change(ChangeType.CHALLENGE, request.user):
                response.__setitem__('X-Achieved-Reward', helper.get_header())

        return response


#Class according to https://docs.djangoproject.com/en/3.0/topics/http/middleware/