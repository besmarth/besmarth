This directory contains images/files that need to be delivered to the production environment along with the source code.
Examples are images that should be installed when the application is taken into operation (e.g. topic level images).
The data can then be installed to the correct place (e.g. MEDIA_ROOT or database).
A practical way to provide installation mechanisms is writing a custom manage.py command, see https://docs.djangoproject.com/en/3.0/howto/custom-management-commands.