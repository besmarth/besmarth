import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def send_mail(challenge_title, challenge_description):
    port = 465  # For SSL
    smtp_server = "smtp.gmail.com"
    sender_email = "besmarth.app@gmail.com"
    receiver_email = "app.besmarth@gmail.com"
    auth_token = 'lqmgiycxwiyavghf'
    message = MIMEMultipart("alternative")
    message["Subject"] = "Approve New Challenge"
    message["From"] = sender_email
    message["To"] = receiver_email

    text = """\
    Hei Eco-Freak\n
    Someone has uploaded a new Challenge. Please approve it on 
    www.besmarth.ch/admin"""
    html = f"""\
    <html>
      <body>
        <p>Hei Eco-Freak<br>
          Someone has uploaded a new Challenge.<br>
        <p>
        <b>Title:</b> </br>
        {challenge_title}<br>
        <b>Description:</b><br>
        {challenge_description}
        </p>
          Please approve it on
           <a href="http://www.besmarth.ch/admin">Besmarth Admin</a> </br>
           Thanks and live sustainable!
        </p>
      </body>
    </html>
    """

    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    message.attach(part1)
    message.attach(part2)

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, auth_token)
        server.sendmail(
            sender_email, receiver_email, message.as_string()
        )
