from django.urls import reverse

from .bne_base import BneBaseTest
from ..models import Topic


class TopicTestCase(BneBaseTest):

    def test_get(self):

        # Setup challenge
        test_internal_id = 0
        test_topic_name = 'Energie'
        test_topic_image_l1 = Topic._img_path + 'my_image.png'

        topic0 = Topic(internal_id=test_internal_id, topic_name=test_topic_name, image_level1=test_topic_image_l1)
        topic1 = Topic(internal_id=test_internal_id + 1, topic_name=test_topic_name + '1',
                       image_level1=test_topic_image_l1 + '1')

        self.supply_objects(topic0, topic1)

        url = reverse('topics-list')

        # Retrieve data
        response_list = self.get_response(url);

        element = response_list.data['results'][0];
        response2 = self.get_response(url + '1/')

        # Assert that elements are the same
        self.assertEquals(response2.data, element)