from django.urls import reverse

from .bne_base import BneBaseTest
from ..models import Topic


class TestNewChallengeView(BneBaseTest):

    def setUp(self):
        super().setUp()

        test_topic_image_l1 = Topic._img_path + 'my_image.png'
        topic0 = Topic.objects.create(internal_id=0, topic_name='Topic1', image_level1=test_topic_image_l1,
                                      topic_description='whuat')

    def test_New_Challenge(self):
        """
        # Valid Data
        data = {
            "title": "No Waste",
            "description": "Generiere keinen müll für 10 Tage",
            "duration": 10,
            "color": "#0B85B5",
            "difficulty": "EASY",
            "periodicity": "DAILY",
            "topic": 0,
            "category": "SHOPPING"
        }
        response = self.client.post(reverse('challenge-add'), data)
        self.assertEqual("New Challenge Added", response.data)
        self.assertEqual(201, response.status_code)
        """

        # Title Is missing
        data = {
            "description": "Generiere keinen Müll für 10 Tage",
            "duration": 10,
            "color": "#0B85B5",
            "difficulty": "EASY",
            "periodicity": "DAILY",
            "topic": 0,
            "category": "SHOPPING"
        }

        response = self.client.post(reverse('challenge-add'), data)
        self.assertEqual(400, response.status_code)
        self.assertEqual("Invalid Request", response.data)

        # Topic does not exist
        data = {
            "title": "ChallengeTitle",
            "description": "Generiere keinen Müll für 10 Tage",
            "duration": 10,
            "color": "#0B85B5",
            "difficulty": "EASY",
            "periodicity": "DAILY",
            "topic": 77,
            "category": "SHOPPING"
        }

        response = self.client.post(reverse('challenge-add'), data)
        self.assertEqual(400, response.status_code)
        self.assertEqual("Invalid Request", response.data)