from django.test import TestCase
from django.urls import reverse

from ..models import Topic, Challenge


class TestNewChallengeView(TestCase):

    def setUp(self):
        super().setUp()
        t1 = Topic.objects.create(internal_id=1, topic_name='Topic 1', image_level1='img_level1.png')
        t2 = Topic.objects.create(internal_id=2, topic_name='Topic 2', image_level1='img_level2.png')

        Challenge.objects.create(title="A very ni12ce challenge", difficulty="EASY", duration=10,
                                 description="Lorem ipsum dolorus", topic=t1, rating="4",
                                 periodicity="WEEKLY", approved=True)
        Challenge.objects.create(title="Yet another nice challenge", difficulty="HARD", description="Lorem ipsum",
                                 duration=10, topic=t2, rating="3",
                                 periodicity="DAILY", approved=True)

    def testFilterChallenge(self):
        response = self.client.get(reverse('filter-challenge') + '?difficulty=EASY')
        self.assertEqual(response.status_code, 200)

        data = '[OrderedDict([(\'id\', 1), ' \
               '(\'title\', \'A very ni12ce challenge\'), ' \
               '(\'description\', \'Lorem ipsum dolorus\'), ' \
               '(\'icon\', \'http://testserver/filterchallenges/?difficulty=EASY\'), ' \
               '(\'image\', \'http://testserver/filterchallenges/?difficulty=EASY\'), ' \
               '(\'duration\', 10), ' \
               '(\'color\', \'\'), ' \
               '(\'rating\', \'4.0\'), ' \
               '(\'difficulty\', \'EASY\'), ' \
               '(\'periodicity\', \'WEEKLY\'), ' \
               '(\'topic\', \'Topic 1\'), ' \
               '(\'category\', \'MOBILITY\')])]'
        self.assertEqual(str(response.data), data)

        # Test that Challenge 1 is not returned when difficulty is HARD
        response = self.client.get(reverse('filter-challenge') + '?difficulty=HARD')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(str(response.data), data)
        # self.assertEqual(len(response.data), 1)

        # Test no challenge is returned, because none of them meets both parameters
        response = self.client.get(reverse('filter-challenge') + '?difficulty=HARD&topic=Topic 1')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

        # Test that an undefined topic leads to a "400 Bad Request" response
        response = self.client.get(reverse('filter-challenge') + '?&topic=AnythingExpectTopic1or2')
        self.assertEqual(response.status_code, 400)
