
from ..models import Topic, Challenge, ChallengeRating, Account
from .bne_base import BneBaseTest
from ..Rating.RatingCalculator import RatingCalculator


class TestRatingManagement(BneBaseTest):

    def setUp(self):
        super().setUp()
        self.ratingCalculator = RatingCalculator()
        t1 = Topic.objects.create(internal_id=1, topic_name='Topic 1', image_level1='img_level1.png')
        t2 = Topic.objects.create(internal_id=2, topic_name='Topic 2', image_level1='img_level2.png')

        self.c1 = Challenge.objects.create(title="A very ni12ce challenge", difficulty="EASY", duration=10,
                                      description="Lorem ipsum dolorus", topic=t1, rating="4",
                                      periodicity="WEEKLY")
        self.c2 = Challenge.objects.create(title="Yet another nice challenge", difficulty="HARD", description="Lorem ipsum",
                                      duration=10, topic=t2, rating="3",
                                      periodicity="DAILY")

        self.lukas = Account(username="TestUser2", first_name='TestUser2')
        self.supply_objects(self.lukas)

    def testAverage(self):

        # Create 2 Ratings
        ChallengeRating.objects.create(challenge=self.c1, rating=5, user_id=1)
        ChallengeRating.objects.create(challenge=self.c1, rating=3, user_id=2)

        self.ratingCalculator.update(1)

        # Check Average rating of rated challenges
        self.assertEqual(Challenge.objects.get(id=1).rating, 4)

