from django.urls import reverse
from rest_framework import status

from .bne_base import BneBaseTest
from ..models import Account, Friendship


class TestAccountFriendsView(BneBaseTest):

    def setUp(self):
        super().setUp()
        self.friend1 = Account(username="friend1")
        self.friend2 = Account(username="friend2")
        self.not_friend1 = Account(username="not_friend1")
        self.not_friend2 = Account(username="not_friend2")
        self.supply_objects(self.friend1,
                            self.friend2,
                            self.not_friend1,
                            self.not_friend2)

        self.friendship1 = Friendship(sender=self.test_user, receiver=self.friend1)
        self.friendship2 = Friendship(sender=self.test_user, receiver=self.friend2)
        self.friendship3 = Friendship(sender=self.not_friend1, receiver=self.not_friend2)
        self.supply_objects(self.friendship1,
                            self.friendship2,
                            self.friendship3)

    def assert_contains_account(self, accounts, account_username, contained=True):
        friend_exists = False
        for friend in accounts:
            if friend["username"] == account_username:
                friend_exists = True
        if contained:
            self.assertTrue(friend_exists)
        else:
            self.assertFalse(friend_exists)

    def test_read_account_friends(self):
        account_friends_url = reverse('account-friends')
        response = self.get_response(account_friends_url)

        self.assert_contains_account(response.data, self.friend1.username)
        self.assert_contains_account(response.data, self.friend2.username)
        self.assert_contains_account(response.data, self.not_friend1.username, False)

    def test_delete_account_friend(self):
        url = reverse('account-friends-edit', args=[self.friend1.id])

        response = self.client.delete(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assert_contains_account(response.data, self.friend1.username, False)
        self.assert_contains_account(response.data, self.friend2.username)
        self.assert_contains_account(response.data, self.not_friend1.username, False)

    def test_delete_account_friend_id_not_exists(self):
        url = reverse('account-friends-edit', args=[1234])

        response = self.client.delete(url)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_delete_account_friend_wrong_ownership(self):
        url = reverse('account-friends-edit', args=[self.not_friend1.id])

        response = self.client.delete(url)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)