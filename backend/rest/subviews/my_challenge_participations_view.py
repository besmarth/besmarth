from rest_framework import generics, authentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ..models import ChallengeParticipation
from ..serializers import ChallengeSerializer, ChallengeParticipationSerializer


class MyChallengeParticipationsView(generics.ListAPIView):
    serializer_class = ChallengeSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        participations = ChallengeParticipation.objects.filter(user=request.user, mark_deleted=False)

        # order by progress_loggable (true before false)
        participations = sorted(participations, key=lambda p: not p.progress_loggable)

        serializer = ChallengeParticipationSerializer(participations, many=True, context={'request': request})
        return Response(serializer.data)
