from django.contrib import admin

from .models import Tip, TipSchedule, Challenge, Topic, Account, Reward, AchievedReward, TopicLevel, ChallengeRating

# Register your models here.

admin.site.register(Tip)
admin.site.register(TipSchedule)
admin.site.register(Challenge)
admin.site.register(Account)
admin.site.register(Topic)
admin.site.register(Reward)
admin.site.register(AchievedReward)
admin.site.register(TopicLevel)
admin.site.register(ChallengeRating)
