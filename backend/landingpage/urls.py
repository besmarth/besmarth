from django.urls import path
from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.conf import settings

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='landingpage/index.html')),
    url(r'^privacy-policy', TemplateView.as_view(template_name='privacy-policy.html'))
]