Release 1
--------------
:Date: October 28, 2020

* No major changes. Familiarization with the besmarth project. Therefore, no changes have been added to the develop branch yet.


Release 2
--------------
:Date: November 27, 2020

* VT342001-69-user-can-filter-challenges: First implementation of filtering challenges
* VT342001-82_add_new_challenge_screen_to_create_a_new_challenge/ VT342001-14_As_a_user_I_can_add_a_new_challenge_so_that_other_users_profit_from_my_knowledge: Users can now create challenges
* VT342001-12-user-has-an-overview-of-the-challenges: Clear screen with all challenges available
* VT342001-37_Improve_the_Home_Screen: New and clear home screen with my started challenges


Release 3
--------------
:Date: January 13, 2021

* VT342001-27_star_rating_for_challenges: Challenges can be rated with a star rating
* VT342001-110_create_challenge_guidance_and_information: There is an intuitive guide to creating a challenge
* VT342001-17_As_a_user_I_can_change_challenge-settings_to_customize_challenges: Challenge duration and frequency can be adjusted when participating in a challenge
* VT342001-69-user-can-filter-challenges: Challenges can be filtered and displayed according to various criteria


Release 4
--------------
:Date: April 14, 2021

* VT342001-184_Filter_Screen_Tags: Full filter functionality with selecting and deselecting filter tags
* VT342001-190_topic_changes: New topics in the application
* VT342001-143_start_challenge_multiple_times: Challenges can now be done multiple times
* VT342001-29_comment_challenge: Challenges can be commented. Comments are also visibly listed per challenge.
* VT342001-175: Bugfixing and stabilization of the app, Expo update issues solved.


Release 5
--------------
:Date: May 07, 2021

* VT342001-163_general_design: New revised coherent design
* VT342001-213_filtering_only_by_filterScreen: Filter process revised so that only the filter screen is used
* VT342001-214_detail_screen_clear: Detail screen adapted according to new design concept
* VT342001-206_fix_frontend_tests: Front-end tests adjusted to avoid errors. Non-functioning tests commented out and described.
* VT342001-210_clear_navigation: More user-friendly button bar implemented, which supports navigation
* VT342001-215: Bugfixing for ios problems
* Additional information on the status of frontend testing: Certain tests do not work with the current test library at the moment (e.g. the call fireEvent -> press does not always work). Tests that do not work have been commented out and documented accordingly.

Release 6
--------------
:Date: June 09, 2021

* VT342001-201_homescreen_good_impression: HomeScreen revised and filled with information about the app.
* VT342001-227_changelog: Added a CHANGELOG.rst
* VT342001-225_privacy_policy: Added a privacy policy web page