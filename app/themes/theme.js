import {DefaultTheme} from 'react-native-paper';


export default {
  ...DefaultTheme,
  roundness: 4,
  colors: {
    ...DefaultTheme.colors,
    primary: '#008534',
    accent: '#F1E54C',
    background: 'white'
  },

};
