import React from "react";
import {createEmptyTestStore, createNetworkMock, renderScreenForTest} from "../../test/test-utils";
import HomeScreen from "../Home/HomeScreen";

const mock = createNetworkMock();


describe("HomeScreen", () => {
    it(`renders with no participations`, async () => {
        mock.reset();
        mock.onGet("/account/participations").reply(200, []);

        const {asJSON} = renderScreenForTest(HomeScreen, createEmptyTestStore());
        expect(asJSON()).toMatchSnapshot();
    });

    it(`renders with participations`, async () => {
        mock.reset();
        mock.onGet("/account/participations").reply(200, [
            {
                "id": 7,
                "joined_time": "2019-12-27T20:32:18.667597+01:00",
                "challenge": {
                    "id": 1,
                    "title": "Veggie day",
                    "description": "Suche dir einen Tag in der Woche aus, an dem du auf Fleisch verzichtest.",
                    "icon": "http://localhost:8000/media/images/challenges/Screenshot_2019-04-03_at_08.15.53_7W1iHZE_al5fTIz.png",
                    "image": "http://localhost:8000/media/images/challenges/healthy2.jpg",
                    "duration": 10,
                    "color": "#ED9118",
                    "difficulty": "EASY",
                    "periodicity": "DAILY",
                    "topic": "Transport"
                },
                "progress": [],
                "shared": false,
                "mark_deleted": false,
                "progress_loggable": true
            },
            {
                "id": 6,
                "joined_time": "2019-12-11T20:52:50.403452+01:00",
                "challenge": {
                    "id": 4,
                    "title": "Shut Exe",
                    "description": "Schalte den Monitor aus, statt den Bildschirmschoner einzustellen.",
                    "icon": "http://localhost:8000/account/participations",
                    "image": "http://localhost:8000/media/images/challenges/shut-eye.jpeg",
                    "duration": 60,
                    "color": "#0B85B5",
                    "difficulty": "EASY",
                    "periodicity": "DAILY",
                    "topic": "Essen"
                },
                "progress": [
                    {
                        "id": 28,
                        "create_time": "2019-12-11T20:52:54.202278+01:00",
                        "participation": 6,
                        "mark_deleted": false
                    },
                    {
                        "id": 29,
                        "create_time": "2019-12-27T20:32:22.180731+01:00",
                        "participation": 6,
                        "mark_deleted": false
                    }
                ],
                "shared": false,
                "mark_deleted": false,
                "progress_loggable": false
            }
        ]);

        const {asJSON} = renderScreenForTest(HomeScreen, createEmptyTestStore());
        expect(asJSON()).toMatchSnapshot();
    })
});

