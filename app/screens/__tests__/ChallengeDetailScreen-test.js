import React from "react";
import ChallengeDetailScreen from "../ChallengeDetailScreen";
import {createEmptyTestStore, createNetworkMock, renderScreenForTest} from "../../test/test-utils";
import {waitForElement,} from "@testing-library/react-native";

const mock = createNetworkMock();

const challenge = {
  id: 134515,
  title: "Challenge Title",
  image: "http://test.local/image/url",
  color: "#abcdef",
  periodicity: "daily",
  duration: 100,
  difficulty: "HARD"
};

const challenge2 = {
  id: 1,
  title: "challenge Title 2344",
  image: "http://test.local/image/url2",
  color: "#abcdef",
  periodicity: "daily",
  duration: 4,
  difficulty: "EASY"
};

describe("ChallengeDetailScreen", () => {
  it(`renders signed up`, async () => {
    mock.reset();
    mock.onGet("/challenges/134515/participation/").reply(200, {
      mark_deleted: false,
      joined_time: "2019-11-29T09:37:35.725283+01:00"
    });
    const store = createEmptyTestStore();
    const {getByTestId, asJSON} = renderScreenForTest(ChallengeDetailScreen, store, {challenge});
    waitForElement(() => getByTestId("Challenge deabonnieren"));
    expect(asJSON()).toMatchSnapshot();
  });

  it(`renders signed out`, async () => {
    const store = createEmptyTestStore();
    mock.reset();
    mock.onPost("/challenges/134515/participation/").reply(404, null);
    const {getByTestId, asJSON} = renderScreenForTest(ChallengeDetailScreen, store, {challenge});
    waitForElement(() => getByTestId("Für Challenge anmelden"));
    expect(asJSON()).toMatchSnapshot();
  });

  it("signs up challenge", async () => {
    mock.reset();
    mock.onGet("/challenges/134515/participation/").reply(404, null);
    mock.onPost("/challenges/134515/participation/").reply(200, {
      joined_time: "2019-11-29T09:37:35.725283+01:00"
    });

    const store = createEmptyTestStore();
    const {getByLabelText, asJSON} = renderScreenForTest(ChallengeDetailScreen, store, {challenge});

    // TODO Does not work with the current test library at the moment. The fireEvent -> press call is not working.
    // const anmelden = waitForElement(() => getByLabelText("Challenge anmelden"));
    // fireEvent.press(anmelden);
    // await waitForElementToBeRemoved(anmelden);
    // const action = waitForReduxAction("SHOW_TOAST");
    // expect(asJSON()).toMatchSnapshot();
  });

  it("log progress of challenge", async () => {
    mock.reset();
    mock.onGet("/challenges/134515/participation/").reply(200, {
      joined_time: "2019-11-29T09:37:35.725283+01:00",
      challenge,
      shared: false,
      mark_deleted: false,
      progress: []
    });
    mock.onPost("/challenges/134515/participation/progress/").reply(200, {
      joined_time: "2019-11-29T09:37:35.725283+01:00",
      mark_deleted: false,
      shared: false,
      progress: [
        {
          create_time: "2019-11-29T09:37:35.725283+01:00"
        }
      ],
      challenge
    });

    const store = createEmptyTestStore();
    const {getByTestId, asJSON} = renderScreenForTest(ChallengeDetailScreen, store, {challenge});

    // TODO Does not work with the current test library at the moment. The fireEvent -> press call is not working.
    // const erledigt = waitForElement(() => getByTestId("Challenge als erledigt markieren"));
    // fireEvent.press(erledigt);
    // waitForElementToBeRemoved(() => getByTestId("Challenge als erledigt markieren"));
    // expect(asJSON()).toMatchSnapshot();

  });

  it("show number of completed challenges", async () => {
    mock.reset();
    mock.onGet("/challenges/134515/participation/").reply(200, {
      id: "7",
      challenge: challenge2,
      joined_time: "2019-10-01T09:37:35.725283+01:00",
      mark_deleted: false,
      progress: [
        {
          id: 1,
          create_time: "2019-11-29T09:37:35.725283+01:00",
          participation: 1,
          mark_deleted: false
        },
        {
          id: 2,
          create_time: "2019-11-30T09:37:35.725283+01:00",
          participation: 1,
          mark_deleted: false
        },
        {
          id: 3,
          create_time: "2019-12-01T09:37:35.725283+01:00",
          participation: 1,
          mark_deleted: false
        },
        {
          id: 4,
          create_time: "2019-12-02T09:37:35.725283+01:00",
          participation: 1,
          mark_deleted: false
        }
      ],
      shared: false,
      progress_loggable: true
    });

    const store = createEmptyTestStore();
    const {getByLabelText, asJSON} = renderScreenForTest(ChallengeDetailScreen, store, {challenge});
    waitForElement(() => getByLabelText("Challenge deabonnieren"));
    expect(asJSON()).toMatchSnapshot();
  });
});
