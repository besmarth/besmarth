import React from "react";
import CreateChallengeScreen from "../CreateChallengeScreen";
import {createEmptyTestStore, renderScreenForTest} from "../../test/test-utils";
import {fireEvent, waitForElement} from "@testing-library/react-native";


describe("ChallengeDetailScreen", () => {
    it("test state of screen after initalisation", async () => {
        const store = createEmptyTestStore();
        const {asJSON} = renderScreenForTest(CreateChallengeScreen, store);
        expect(asJSON()).toMatchSnapshot();
    });

    it("input fields after creation in initial state", async () => {

        const store = createEmptyTestStore();

        const {getByTestId, getByLabelText, asJSON} = renderScreenForTest(CreateChallengeScreen, store);

        const title = await waitForElement(() => getByLabelText("Challenge Name"));
        fireEvent.changeText(title, "TEST");

        const desc = await waitForElement(() => getByLabelText("Challenge Description"));
        fireEvent.changeText(desc, "TEST");

        const dur = await waitForElement(() => getByLabelText("Challenge Duration"));
        fireEvent.changeText(dur, "15");

        fireEvent.press(getByTestId("create-button"));

        expect(asJSON()).toMatchSnapshot();
    });
    
});
