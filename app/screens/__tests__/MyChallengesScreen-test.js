import React from "react";
import {createEmptyTestStore, createNetworkMock, renderScreenForTest} from "../../test/test-utils";
import MyChallengesScreen from "../MyChallengesScreen";
import {fireEvent, waitForElement} from "@testing-library/react-native";

const mock = createNetworkMock();

function mockSetup() {
    mock.reset();
    challenge_template.topic = "Energie";
    mock.onGet("/challenges/topic/1/").reply(200, [
        challenge_template, challenge_template
    ]);
    challenge_template.topic = "Transport";
    mock.onGet("/challenges/topic/2/").reply(200, [
        challenge_template, challenge_template
    ]);
    challenge_template.topic = "Abfall";
    mock.onGet("/challenges/topic/3/").reply(200, [
        challenge_template, challenge_template
    ]);
    challenge_template.topic = "Konsum";
    mock.onGet("/challenges/topic/4/").reply(200, [
        challenge_template, challenge_template
    ]);
}

let challenge_template = {
    id: 1,
    title: "Hier Titel einfügen",
    description: "Irgendeinen Text als Ersatz für die Beschreibung",
    icon: "http://test.local10/challenges/topic/1/",
    image: "http://test.local10/image/url",
    duration: 42,
    color: "#0B85B5",
    difficulty: "EASY",
    periodicity: "DAILY",
    category: "MOBILITY",
    topic: "NeedsToBeSet"
};


describe("MyChallengesScreen", () => {
    it("renders empty challenges list of topic 1", async () => {
        mock.reset();
        mock.onGet("/challenges/topic/1/").reply(200, []);
        mock.onGet("/challenges/topic/2/").reply(200, []);
        mock.onGet("/challenges/topic/3/").reply(200, []);
        mock.onGet("/challenges/topic/4/").reply(200, []);

        const {getByTestId, asJSON} = renderScreenForTest(MyChallengesScreen, createEmptyTestStore());
        await waitForElement(() => getByTestId("challenges-list-topic-1"));
        await waitForElement(() => getByTestId("challenges-list-topic-2"));
        await waitForElement(() => getByTestId("challenges-list-topic-3"));
        await waitForElement(() => getByTestId("challenges-list-topic-4"));
        expect(asJSON()).toMatchSnapshot();
    });


    it("renders 2 challenges per topic", async () => {
        mockSetup();

        const {getByTestId, asJSON} = renderScreenForTest(MyChallengesScreen, createEmptyTestStore());
        await waitForElement(() => getByTestId("challenges-list-topic-1"));
        await waitForElement(() => getByTestId("challenges-list-topic-2"));
        await waitForElement(() => getByTestId("challenges-list-topic-3"));
        await waitForElement(() => getByTestId("challenges-list-topic-4"));
        expect(asJSON()).toMatchSnapshot();
    });

    it("user opens filter tab", async () => {
        mockSetup();

        const store = createEmptyTestStore();
        const {getByTestId, asJSON} = renderScreenForTest(MyChallengesScreen, store);
        await waitForElement(() => getByTestId('filter-button'));
        fireEvent.press(getByTestId('filter-button'));
        expect(asJSON()).toMatchSnapshot();

        // await waitForElement(() => getByTestId('search-button'));
        // fireEvent.press(getByTestId('search-button'));
        // expect(asJSON()).toMatchSnapshot();
    });

    it("user opens new", async () => {
        mockSetup();

        const store = createEmptyTestStore();
        const {getByTestId, getByLabelText, getByText, asJSON} = renderScreenForTest(MyChallengesScreen, store);
        await waitForElement(() => getByTestId('new-button'));
        fireEvent.press(getByTestId('new-button'));
        expect(asJSON()).toMatchSnapshot();
    });

});

