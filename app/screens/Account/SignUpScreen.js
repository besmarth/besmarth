import React from 'react';
import {KeyboardAvoidingView, StyleSheet} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import AccountForm from "../../components/AccountForm";

class SignUpScreen extends React.Component {

  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        <AccountForm signUpMode={true} submitTitle="Abschliessen"/>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);