import React from "react";
import {KeyboardAvoidingView, ScrollView, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {signIn} from "../../services/Auth";
import {ActivityIndicator, Button, TextInput} from "react-native-paper";
import ErrorBox from "../../components/ErrorBox";
import Navigation from "../../services/Navigation";
import {showToast} from "../../services/Toast";

class SignInScreen extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  async onSubmit() {
    try {
      this.setState({...this.state, loading: true});
      const account = await this.props.signIn(this.state.username, this.state.password);
      Navigation.goBack();
      Navigation.navigate("HomeTab", "Home");
      this.setState({...this.state, loading: false, errors: null});
      this.props.showToast("Willkommen zurück, " + account.first_name);
    } catch (e) {
      console.log(e);
      this.setState({
        ...this.state,
        loading: false,
        errors: e.data
      });
    }
  }

  onChangeField(propertyName, newValue) {
    this.setState({
      ...this.state,
      [propertyName]: newValue
    });
  }

  render() {
    if (this.state.loading) {
      return <View style={[styles.container, {alignItems: "center", justifyContent: "center"}]}><ActivityIndicator
        animating={true} size="large"/></View>;
    } else {
      return (
        <KeyboardAvoidingView style={styles.container}>
          <ScrollView keyboardShouldPersistTaps="handled" style={styles.container}>
            <ErrorBox errors={this.state.errors}></ErrorBox>
            <TextInput
              style={styles.formControl}
              label='Benutzername'
              autoCapitalize="none"
              accessibilityLabel="Benutzername"
              value={this.state.username}
              onChangeText={val => this.onChangeField("username", val)}
            />

            <TextInput
              label='Passwort'
              style={styles.formControl}
              accessibilityLabel="Passwort"
              secureTextEntry={true}
              value={this.state.password}
              onChangeText={val => this.onChangeField("password", val)}
            />
            <Button style={styles.formControl} mode="contained" accessibilityLabel="Login"
                    onPress={this.onSubmit.bind(this)}>Login</Button>
          </ScrollView>
        </KeyboardAvoidingView>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  formControl: {
    marginVertical: 10
  }
});

const mapStateToProps = state => {
  return {};
};

const
  mapDispatchToProps = dispatch => bindActionCreators({
    signIn,
    showToast
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);