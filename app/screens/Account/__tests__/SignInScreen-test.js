import React from "react";
import SignInScreen from "../SignInScreen";
import {fireEvent, waitForElement} from "@testing-library/react-native";
import {createEmptyTestStore, createNetworkMock, renderScreenForTest} from "../../../test/test-utils";


const mock = createNetworkMock();

describe("SignInScreen", () => {
  it(`renders`, () => {
    const {asJSON} = renderScreenForTest(SignInScreen, createEmptyTestStore());
    expect(asJSON()).toMatchSnapshot();
  });

  it(`logs user in`, async () => {
    mock.reset();
    mock.onPost("/account", {username: "skeeks", password: "testpw"}).reply(200, {
      username: "skeeks",
      token: "theauthtoken",
      first_name: "Samuel",
      email: "email@web.com"
    });

    const store = createEmptyTestStore();
    const {getByLabelText, asJSON} = renderScreenForTest(SignInScreen, store);

    const usernameField = await waitForElement(() => getByLabelText("Benutzername"));
    fireEvent.changeText(usernameField, "skeeks");
    const passwordField = await waitForElement(() => getByLabelText("Passwort"));
    fireEvent.changeText(passwordField, "testpw");
    expect(asJSON()).toMatchSnapshot(); // additional snapshot

    //TODO The fireEvent-call "press" is not working with this testing-library version 5.0.1
    /*
    const btn = await waitForElement(() => getByLabelText("Login"));
    fireEvent.press(btn);


    await waitForReduxAction("ACCOUNT_UPDATE");
    expect(mock.history.post.length).toBe(1);
    expect(store.getState().account).toEqual({
      loading: false,
      account: {
        username: "skeeks",
        token: "theauthtoken",
        first_name: "Samuel",
        email: "email@web.com"
      }
    });
    expect(RemoteStorage.API.headers["Authorization"]).toBe("Token theauthtoken");
    */
  });
});