import React from "react";
import {StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {signUpChallenge} from "../services/Challenges";
import {connect} from "react-redux";
import {Button, Text, TextInput} from "react-native-paper";
import {showToast} from "services/Toast";
import SwitchSelector from "react-native-switch-selector";
import Navigation from "../services/Navigation";
import * as Colors from "../styles/colors";
import Toast from "react-native-toast-message";

class CustomizeChallengeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    componentDidMount() {
        this.reload().then();
    }


    async reload() {
        const c = this.props?.route?.params?.challenge;
        this.setState({challenge: c});
        let dur = '' + c.duration;
        this.setState({duration: dur});
        this.setState({frequency: c.periodicity});
        if (c.periodicity === 'DAILY') {
            this.setState({durationText: '(Anzahl Tage)'});
        } else if (c.periodicity === 'WEEKLY') {
            this.setState({durationText: '(Anzahl Wochen)'});
        } else {
            this.setState({durationText: '(Anzahl Monate)'});
        }
    }

    async onSignUpChallenge() {
        const result = await signUpChallenge(this.state.challenge, this.state.duration, this.state.frequency);
        this.props.route.params.reloadView();
        Navigation.push("MyChallengesTab", "ChallengeDetail", {
            challenge: this.state.challenge,
            participation: result
        });
        Toast.show({
            text2: "Gratuliere! Du hast dich erfolgreich für " + this.state.challenge.title + " angemeldet",
            position: 'bottom', type: 'success',
        });

    }

    onTextChanged(text) {
        this.setState({duration: text})
    }

    render() {
        let periodicity = this.props?.route?.params?.challenge.periodicity;
        let initFrequency = 0;
        if (periodicity !== undefined) {
            if (periodicity === 'WEEKLY') {
                initFrequency = 1
            } else if (periodicity === 'MONTHLY') {
                initFrequency = 2
            }
        }

        return (
            <View style={styles.container}>
                <Text style={styles.labelTitle}>Dauer {this.state.durationText}</Text>
                <TextInput keyboardType='numeric'
                           onChangeText={(text) => this.onTextChanged(text)}
                           value={this.state.duration}>
                </TextInput>

                <Text style={styles.labelTitle}>Häufigkeit</Text>

                <SwitchSelector
                    initial={initFrequency}
                    textColor='#000000'
                    selectedColor='#FFFFFF'
                    buttonColor='#8FC9B9'
                    borderColor='#8FC9B9'
                    hasPadding
                    options={[
                        {label: "Täglich", value: "DAILY"},
                        {label: "Wöchentlich", value: "WEEKLY"},
                        {label: "Monatlich", value: "MONTHLY"}
                    ]}
                    onPress={value => {
                        this.setState({frequency: value});
                        if (value === 'DAILY') {
                            this.setState({durationText: '(Anzahl Tage)'});
                        } else if (value === 'WEEKLY') {
                            this.setState({durationText: '(Anzahl Wochen)'});
                        } else {
                            this.setState({durationText: '(Anzahl Monate)'});
                        }
                    }}
                    style={styles.switch}
                />

                <Button
                    label="Teilnehmen"
                    accessibilityLabel="Für Challenge anmelden"
                    style={styles.button}
                    mode="contained"
                    onPress={this.onSignUpChallenge.bind(this)}>Teilnehmen</Button>
            </View>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
    },
    labelTitle: {
        margin: 10,
        fontWeight: 'bold'
    },
    switch: {
        margin: 5,
        marginBottom: 15
    },
    button: {
        borderColor: Colors.green,
        borderWidth: 1,
        backgroundColor: Colors.green,
        color: 'white',
        marginHorizontal: 10,
        marginVertical: 10,
    },


});

const mapStateToProps = state => {
    return {};
};
const mapDispatchToProps = dispatch => bindActionCreators({
    showToast,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CustomizeChallengeScreen);
