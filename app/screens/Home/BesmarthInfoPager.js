import {Image, StyleSheet, Text, View, Linking, Button} from "react-native";
import React from "react";
import {Colors, Typography} from "../../styles";
import TabBarIcon from "../../components/TabBarIcon";
import PagerView from 'react-native-pager-view';
import Dots from 'react-native-dots-pagination';
import RemoteStorage from "../../services/RemoteStorage";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import theme from "../../themes/theme";
import {Ionicons} from "@expo/vector-icons";

/**
 * Shows infos about Besmarth App
 *
 * Use like <BesmarthInfoPager />
 */
export class BesmarthInfoPager extends React.Component {

    constructor() {
        super();
        this.state = {
            active: 0,
            isShown: true
        };
    }

    setActivePage(activePage) {
        if(activePage !== this.state.active) {
            this.setState({active: activePage})
        }
    }

    render() {
        return (
            <View style={{height: 340}}>
                <PagerView style={styles.pagerView}
                           showPageIndicator={false}
                           onPageSelected = {(e) => { this.setActivePage(e.nativeEvent.position) } }
                           pageMargin={0}
                >
                    <View style={styles.pagerPageContainer} key="0">
                        <View style={styles.pageCard}>
                            <View style={{flexDirection:'column',flex:2, justifyContent: "flex-start"}}>
                                <Text style={[Typography.headerText, {paddingBottom: 10}]}>Intro Besmarth App</Text>
                                {/*
                                HOME Tab
                                */}
                                <View style={styles.tabInfoRow}>
                                    <TabBarIcon
                                        name='home'
                                        focused={true}
                                    />
                                    <Text style={[Typography.bodyText, {transform: [{translateY: 2}]}]}>  Deine Challenges anschauen</Text>
                                </View>
                                {/*
                                Challenges Tab
                                */}
                                <View style={styles.tabInfoRow}>
                                    <Ionicons
                                        name='trophy'
                                        size={26}
                                        style={{paddingTop: 6}}
                                        color={theme.colors.primary}
                                    />
                                    <View style={{flex: 1, flexDirection: "column", paddingLeft: 10}}>
                                        <View style={{flexDirection: "row"}}>
                                            <Icon name="compass-outline" size={20} color={theme.colors.primary}/>
                                            <Text style={Typography.bodyText}>  Challenges entdecken</Text>
                                        </View>
                                        <View style={{flexDirection: "row"}}>
                                            <Icon name="plus-box" size={20} color={theme.colors.primary} />
                                            <Text style={Typography.bodyText}>  Challenges für die Community kreieren</Text>
                                        </View>
                                    </View>
                                </View>
                                {/*
                                Friends Tab
                                */}
                                <View style={styles.tabInfoRow}>
                                    <TabBarIcon
                                        name='people-circle-outline'
                                        focused={true}
                                    />
                                    <Text style={[Typography.bodyText, {transform: [{translateY: 2}]}]}>  Freunde einladen und Fortschritt teilen</Text>
                                </View>
                                {/*
                                Account Tab
                                */}
                                <View style={styles.tabInfoRow}>
                                    <TabBarIcon
                                        name='person-circle-outline'
                                        focused={true}
                                    />
                                    <Text style={[Typography.bodyText, {transform: [{translateY: 2}]}]}>  Account erstellen und verwalten</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    {/**
                     Infos about Besmarth
                     */}
                    <View style={styles.pagerPageContainer} key="1">
                        <View style={styles.pageCard}>
                            <Text style={Typography.headerText}>Über Besmarth</Text>
                            <Text style={Typography.bodyText}>Mit der besmarth App kannst du in vielfältigen Challenges teilnehmen, mit dem Ziel bewusster durch den Alltag zu gehen und einen angemessenen Fussabdruck auf unserer Erde zu hinterlassen.</Text>
                            <View style={{flex: 1, alignItems: "flex-end"}}>
                                <Image source={require('../../assets/images/icon.png')} style={styles.icon}/>
                            </View>
                        </View>
                    </View>
                    {/**
                     Privacy Policy
                     */}
                    <View style={styles.pagerPageContainer} key="2">
                        <View style={styles.pageCard}>
                            <Text style={Typography.headerText}>Datenschutzbestimmungen</Text>
                            <View style={{height: 150}}>
                                <Image source={require('../../assets/images/logo.png')} style={styles.logo}/>
                            </View>
                            <Button
                                mode="contained"
                                color={Colors.green}
                                title={'zu den Datenschutzbestimmungen'}
                                onPress={ () => Linking.openURL( RemoteStorage.getBaseUrl() + '/privacy-policy')}
                                testID="privacy-policy-button"
                                style={styles.privacyButton}
                            />
                        </View>
                    </View>
                </PagerView>
                <View style={{transform: [{translateY: -40}]}}>
                    <Dots length={3} activeColor={Colors.green} active={this.state.active} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    pagerView: {
        flex: 1,
    },
    pagerPageContainer: {
        paddingLeft: 14,
        paddingRight: 14,
        paddingBottom: 10,
    },
    pageCard: {
        justifyContent:'flex-start',
        flex: 1,
        flexDirection: 'column',
        borderColor: Colors.green,
        borderWidth: 2,
        padding: 20,
        height: '100%',
        borderRadius: 10,
        backgroundColor: '#EEE',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        }, shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    logo: {
        flex: 1,
        width: null,
        height: 100,
        resizeMode: 'contain',
        marginVertical: 15
    },
    icon: {
        marginTop: 10,
        width: 100,
        height: 100,
        resizeMode: 'contain'
    },
    privacyButton: {
        marginTop: 20
    },
    tabInfoRow: {flexDirection: "row",
        paddingVertical: 10,
        borderBottomColor: Colors.lightGray,
        borderBottomWidth: 1
    }
});
