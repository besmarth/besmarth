import React from "react";
import {Button, FlatList, ScrollView, StyleSheet, Text, View} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchTopics} from "../../services/Topics";
import {Card, TouchableRipple} from "react-native-paper";
import {fetchOwnChallengeParticipations} from "../../services/Challenges";
import * as Colors from "../../styles/colors";
import ProgressCircle from 'react-native-progress-circle'
import Navigation from "../../services/Navigation";
import {notNull} from "../../services/utils";
import RemoteStorage from "../../services/RemoteStorage";
import * as Topics from "../../constants/Topics";
import {BesmarthInfoPager} from "./BesmarthInfoPager";
import theme from "../../themes/theme";

class HomeScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            active: 0
        };
    }

    componentDidMount() {
        this.reload().then();
        this.subscription = this.props.navigation.addListener("focus", this.reload.bind(this));
    }

    async reload() {
        this.setState({loading: true});
        try {
            const challengeParticipations = await fetchOwnChallengeParticipations();
            const accountResponse = await RemoteStorage.get(`/account`);
            const firstName = accountResponse.data.first_name;
            this.setState({
                loading: false,
                challengeParticipations,
                firstName
            });
        } catch (e) {
            console.log(e);
            this.setState({
                error: JSON.stringify(e),
                loading: false
            });
        }
    }

    render() {
        const emptyContent = <View style={styles.emptyListContainer}>
            <Text style={{fontSize: 11, marginBottom: 10}}>Du nimmst leider noch an keinen Challenges teil!</Text>
            <Button
                title={"Entdecken"}
                    style={styles.button}
                    mode="contained"
                    color={theme.colors.primary}
                    onPress={() => Navigation.push("MyChallengesTab", "MyChallenges", {})}/>
        </View>;

        const headerContent = () => {
            return (
                <View>
                    <Text style={styles.titleText1}>Welcome back, {this.state.firstName}!</Text>
                    <Text style={styles.titleText2}>Meine Challenges</Text>
                    <FlatList
                        horizontal={true}
                        contentContainerStyle={{ flexGrow: 1 }}
    disableVirtualization={false}
                        style={styles.participationList}
                        data={this.state.challengeParticipations}
                        onRefresh={this.reload.bind(this)}
                        refreshing={this.state.loading}
                        keyExtractor={(item) => item.id + ""}
                        ListEmptyComponent={() => emptyContent}
                        renderItem={({item}) => <ParticipationCard participation={item}
                                                                   onPressCard={this.onOpenChallenge}/>}
                    />
                </View>
            )
        }

        return (
            <ScrollView style={styles.homeContainer}>
                <FlatList
                    ListHeaderComponent={headerContent}
                    style={styles.feedList}
                    initialNumToRender={20}
                    keyExtractor={(item) => item.name}
                    data={null} /* {FEED_DATA} replace null with FEED_DATA when the feed is implemented*/
                    renderItem={({item}) => {}}
                />
                <BesmarthInfoPager/>
            </ScrollView>
        );
    }

    async onOpenChallenge(participation) {
        Navigation.push("HomeTab", "ChallengeDetail", {challenge: participation.challenge});
    }
}

function ParticipationCard({participation, onPressCard}) {
    const challenge = participation.challenge;
    const achievedCount = participation?.progress?.length || 0;
    let challengeDuration;
    if (participation.duration !== null && participation.duration !== 0) {
        challengeDuration = participation.duration;
    } else {
        challengeDuration = challenge.duration;
    }
    const countCurrentProgress = (achievedCount % challengeDuration);
    const percentage = Math.round(100 / challengeDuration * countCurrentProgress);
    const image = notNull(challenge.image) ? {uri: challenge.image} : require("../../assets/images/no-challenge-image.png");
    // In the moment there are no topic images available in the data base.
    //const topicImage = notNull(challenge.topic.image) ? {uri: challenge.topic.image} : require("../assets/images/no-challenge-image.png");

    return (
        <TouchableRipple onPress={() => onPressCard(participation)}>
            <View style={styles.participationItem}>
                <Card style={styles.cardStyle}>
                    <Card.Content style={{width: 150, alignItems: 'center', alignSelf: "center"}}>
                        <ProgressCircle containerStyle={{alignItems: 'center', alignSelf: "center"}}
                                        percent={percentage}
                                        radius={60}
                                        borderWidth={10}
                                        color={"#008d36"}
                                        shadowColor="#BCE0C8"
                        >
                            <Card.Cover source={image} style={styles.cardCoverStyle}/>
                        </ProgressCircle>
                    </Card.Content>
                    <Text numberOfLines={2} style={styles.challengesTitle}>{participation.challenge.title}</Text>
                </Card>
            </View>
        </TouchableRipple>
    )
}

const styles = StyleSheet.create({
    homeContainer: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 10,
    },
    cardStyle: {
        width: 150,
        height: 205,
        borderRadius: 10,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        }, shadowOpacity: 0.27,
        shadowRadius: 4.65,
        borderColor: Colors.green,
        borderWidth: 2.5,
        elevation: 6,
    },
    feedItem: {
        marginTop: 5,
        marginBottom: 5,
        padding: 5,
        backgroundColor: '#E6D5EA',
        flex: 1,
        alignSelf: "center",
        flexDirection: "row",
        borderRadius: 15
    },
    participationItem: {
        margin: 3,
        padding: 5,
        flex: 1,
        flexDirection: "row",
        width: 160
    },
    titleText1: {
        fontSize: 26,
        fontWeight: '800',
        marginLeft: 10
    },
    titleText2: {
        fontSize: 20,
        fontWeight: '300',
        marginLeft: 10,
        marginTop: 20
    },
    participationList: {
        width: "100%",
        flex: 1,
        marginLeft: 0,
        marginRight: 10,
    },
    emptyListContainer: {
        width: '100%',
        height: 200,
        marginTop: 10,
        marginBottom: 1,
        marginHorizontal: 6,
        flexDirection: 'column',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderStyle: 'solid',
        borderColor: theme.colors.primary,
        borderRadius: 10,
        backgroundColor: '#ccc'
    },
    feedList: {
        flex: 1,
        width: "95%",
        marginLeft: 8,
        marginRight: 8,
        marginBottom: 8
    },
    cardCoverStyle: {
        width: 100,
        height: 100,
        borderRadius: 100/ 2
    },
    challengesTitle: {
        display: "flex",
        textAlign: "center",
        fontSize: 17,
        fontWeight: 'bold',
        width: 140,
        marginTop: 15,
        marginRight: 10,
        marginLeft: 10,
        marginBottom: 10,
        paddingHorizontal: 5,
        paddingBottom: 5
    },
    pagerView: {
        flex: 1,
    },
    pagerPageContainer: {
        paddingLeft: 14,
        paddingRight: 14,
        paddingBottom: 10
    },
    pageCard: {
        borderColor: Colors.green,
        borderWidth: 2,
        padding: 20,
        height: '100%',
        borderRadius: 10,
        backgroundColor: '#EEE',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        }, shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    logo: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'contain'
    },
    icon: {
        marginTop: 10,
        width: 100,
        height: 100,
        resizeMode: 'contain'
    },
    privacyButton: {
        marginTop: 30
    }
});

const mapStateToProps = state => {
    return {
        topics: Object.values(state.topics.topics),
        refreshing: state.topics.refreshing,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        fetchTopics,
    }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);