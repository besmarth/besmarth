import React from "react";
import {FlatList, Image, Platform, ScrollView, StyleSheet, TouchableOpacity, View} from "react-native";
import {bindActionCreators} from "redux";
import * as Colors from "../styles/colors";
import {Headline, IconButton, Surface, Text} from "react-native-paper";
import {connect} from "react-redux";
import {createChallengeProgress, fetchChallenges} from "../services/Challenges";
import Navigation from "../services/Navigation";
import {notNull} from "../services/utils";
import * as Topics from "../constants/Topics";


// NEW All challenges screen
class MyChallengesScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            //for bottom sheet
            visible: false,
            filtered: false,
        };
    }


    componentDidMount() {
        this.reload().then();
        this.subscription = this.props.navigation.addListener("focus", this.reload.bind(this));
        this.props.navigation.setOptions({
            headerRight: () => (
                <View style={{flexDirection: 'row', alignItems: "center"}}>
                    <IconButton
                        icon="plus-box"
                        size={40}
                        padding={10}
                        color={Colors.green}
                        onPress={() => Navigation.push("MyChallengesTab", "CreateChallenge", {})}
                        testID="new-button">
                    </IconButton>
                    <IconButton
                        icon="filter"
                        size={40}
                        padding={10}
                        color={Colors.green}
                        onPress={() =>
                            Navigation.push("MyChallengesTab", "Filtered", {})}
                        testID="filter-button">
                    </IconButton>
                </View>

            )
        });
    }

    componentWillUnmount() {
        this.subscription();
    }

    async reload() {
        this.setState({loading: true});
        try {
            //fetches the challenges by topics
            const topic1 = await fetchChallenges(1);
            const topic2 = await fetchChallenges(2);
            const topic3 = await fetchChallenges(3);
            const topic4 = await fetchChallenges(4);

            this.setState({
                loading: false,
                topic1,
                topic2,
                topic3,
                topic4,
            });
        } catch (e) {
            console.log(e);
            this.setState({
                error: JSON.stringify(e),
                loading: false
            });
        }
    }

    render() {
        if (this.state.error) {
            return <View style={styles.container}><Text>{this.state.error}</Text></View>;
        }

        const emptyContent = <Headline style={styles.emptyListWrapper}>Du nimmst leider noch an keinen Challenges
            teil!</Headline>;


        return <View style={styles.container}>
            <ScrollView>

                    <View style={{flexDirection: 'row'}}>
                        <Text style={styles.mainTitle}>Alle Challenges</Text>
                    </View>
                    <Text style={styles.topicsText}>{Topics.topic1}</Text>
                    <FlatList
                        horizontal={true}
                        testID="challenges-list-topic-1"
                        style={styles.participationList}
                        data={this.state.topic1}
                        onRefresh={this.reload.bind(this)}
                        refreshing={this.state.loading}
                        keyExtractor={(item) => item.id + ""}
                        ListEmptyComponent={() => emptyContent}
                        //ChallengeItem is name of function. Takes two parameters: challenge + onPressItem
                        renderItem={({item}) => <ChallengeItem challenge={item}
                                                               onPressItem={this.onOpenChallenge}/>}
                    />
                    <Text style={styles.topicsText}>{Topics.topic3}</Text>
                    <FlatList
                        horizontal={true}
                        testID="challenges-list-topic-3"
                        style={styles.participationList}
                        data={this.state.topic3}
                        onRefresh={this.reload.bind(this)}
                        refreshing={this.state.loading}
                        keyExtractor={(item) => item.id + ""}
                        ListEmptyComponent={() => emptyContent}
                        renderItem={({item}) => <ChallengeItem challenge={item}
                                                               onPressItem={this.onOpenChallenge}/>}
                    />
                    <Text style={styles.topicsText}>{Topics.topic4}</Text>
                    <FlatList
                        horizontal={true}
                        testID="challenges-list-topic-4"
                        style={styles.participationList}
                        data={this.state.topic4}
                        onRefresh={this.reload.bind(this)}
                        refreshing={this.state.loading}
                        keyExtractor={(item) => item.id + ""}
                        ListEmptyComponent={() => emptyContent}
                        renderItem={({item}) => <ChallengeItem challenge={item}
                                                               onPressItem={this.onOpenChallenge}/>}
                    />
                    <Text style={styles.topicsText}>{Topics.topic2}</Text>
                    <FlatList
                        horizontal={true}
                        testID="challenges-list-topic-2"
                        style={styles.participationList}
                        data={this.state.topic2}
                        onRefresh={this.reload.bind(this)}
                        refreshing={this.state.loading}
                        keyExtractor={(item) => item.id + ""}
                        ListEmptyComponent={() => emptyContent}
                        renderItem={({item}) => <ChallengeItem challenge={item}
                                                               onPressItem={this.onOpenChallenge}/>}
                    />
            </ScrollView>
        </View>;
    }

    //takes you to the challenge detail screen.
    async onOpenChallenge(challengeItem) {
        Navigation.push("MyChallengesTab", "ChallengeDetail", {challenge: challengeItem});
        //see push() -> ../services/Navigation
    }

    async createChallenge() {
        Navigation.push("MyChallengesTab", "CreateChallenge", {});
    }

    async onLogProgress(participation) {
        const updatedParticipation = await createChallengeProgress(participation.challenge);
        this.reload().then();

        if (updatedParticipation.progress.length === updatedParticipation.challenge.duration) {
            this.props.showToast("Gratuliere! Du hast die Challenge " + updatedParticipation.challenge.title + " erfolgreich abgeschlossen");
        }
    }
}


// takes the a challenge and the onOpenChallenge function
function ChallengeItem({challenge, onPressItem}) {

    const image = notNull(challenge.image) ? {uri: challenge.image} : require("../assets/images/no-challenge-image.png");

    return (
        <TouchableOpacity
            // call onOpenChallenge function
            onPress={() => onPressItem(challenge)}
            activeOpacity={0.6}
        >
            <Surface style={styles.participationItem}>
                <View style={styles.itemContent}>
                    <Image source={image} style={styles.cardImageStyle}/>
                    <View style={styles.cardBottomBarContainer}>
                        <Text numberOfLines={1} style={styles.cardTitle}>{challenge.title} </Text>
                        <Text numberOfLines={1} style={{
                            color: 'white',
                            flex: 1.0
                        }}>{challenge.rating > 0 ? "☆" + challenge.rating : "☆ - "}</Text>
                    </View>
                </View>
            </Surface>
        </TouchableOpacity>
    )
}

const cardWidth = 170;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
    },

    participationList: {
        width: "100%",
        height: 190,
        opacity: 1,
    },

    bottomNavigationView: {
        backgroundColor: '#fff',
        width: '100%',
        height: 600,
        alignItems: 'center',
        marginBottom: 20,
    },
    mainTitle: {
        fontWeight: '300',
        fontSize: 30,
        textAlign: "left",
        padding: 8,
    },

    participationItem: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        width: cardWidth,
        height: 185,
        borderRadius: 15,
        margin: 5,
        overflow: 'hidden',
        borderColor: Colors.green,
        borderWidth: 2.5,
    },
    touchableCard: {
        margin: 8,
        marginHorizontal: 6,
        fontSize: 10,
    },

    cardTitle: {
        flex: Platform.OS === "android" ? 3 : 2.4,
        fontSize: 14,
        color: 'white'
    },

    topicsText: {
        marginTop: 10,
        fontWeight: '300',
        textAlign: "left",
        fontSize: 20,
        left: 10,
    },

    item: {
        borderWidth: 1,
        borderColor: '#008D36',
        backgroundColor: '#FFF',
    },
    label: {
        color: '#333'
    },
    itemSelected: {
        backgroundColor: '#008d36',
        borderColor: '#fff',
    },
    labelSelected: {
        color: '#FFF',
    },

    emptyListWrapper: {
        marginTop: "30%",
        marginHorizontal: 10,
        textAlign: "center"
    },

    itemContent: {
        alignItems: "center",
        flexDirection: "column",
        flex: 1,
    },
    cardImageStyle: {
        width: cardWidth,
        height: 140,
        resizeMode: 'cover',
    },
    buttonFilter: {
        marginVertical: 8,
        minWidth: "50%",
        marginBottom: 10
    },

    cardBottomBarContainer: {
        paddingTop: 8,
        paddingLeft: 8,
        paddingRight: 8,
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: Colors.green,
        paddingHorizontal: 4,
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MyChallengesScreen);
