import React, {useEffect, useState} from "react";
import {StyleSheet, Text, View} from "react-native";
import {BarCodeScanner} from "expo-barcode-scanner";
import Navigation from "../../services/Navigation";
import {Button} from "react-native-paper";

export default function ScanFriendCodeScreen(props) {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const {status} = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  const handleBarCodeScanned = ({type, data}) => {
    setScanned(true);
    Navigation.navigate("FriendsTab", "Friends", {scannedCode: data});
  };

  if (hasPermission === null) {
    return <View style={styles.container}>
      <Text>Bitte gib uns Zugriff auf deine Kamera.</Text></View>;
  }
  if (hasPermission === false) {
    return <View style={styles.container} testID="permission-denied-info">
      <Text>Der Zugriff auf die Kamera wurde von dir verweigert.</Text>
    </View>;
  }

  return (
    <View style={styles.scannerContainer}>
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={{flexGrow: 1}}
      />
      <View style={{display: "flex", justifyContent: "center", alignItems: "center", marginBottom: 10}}>
        <Button mode="contained" style={{maxWidth: "50%", minWidth: 100}} color="red"
                onPress={() => Navigation.goBack()} accessibilityLabel="Abbrechen">Abbrechen</Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  scannerContainer: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    backgroundColor: "black"
  }
});

