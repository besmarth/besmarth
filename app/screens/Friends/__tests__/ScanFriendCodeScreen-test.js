import {createEmptyTestStore, renderScreenForTest} from "../../../test/test-utils";
import ScanFriendCodeScreen from "../ScanFriendCodeScreen";
import {waitForElement} from "@testing-library/react-native";
import React, {useEffect} from "react";
import {Text, View} from "react-native";

// mock barcode scanner
function mockedBarCodeScanner(props) {
  useEffect(() => {
    setTimeout(() => {
      props.onBarCodeScanned({type: "qr", data: "testscanresult"});
    }, 20);
  }, []);

  return <View><Text>The barcode scanner</Text></View>;
}


let permissionGranted = true;
// mocks static method requestPermissionsAsync
mockedBarCodeScanner.requestPermissionsAsync = () => {
  return Promise.resolve({status: permissionGranted ? "granted" : "denied"});
};

jest.mock("expo-barcode-scanner", () => ({
  BarCodeScanner: mockedBarCodeScanner
}));

describe("ScanFriendCodeScreen", () => {
  it(`renders asking for permission`, async () => {
    const {asJSON} = renderScreenForTest(ScanFriendCodeScreen, createEmptyTestStore());
    expect(asJSON()).toMatchSnapshot();
  });

  it(`renders with scanner open`, async () => {
    permissionGranted = true;
    const {asJSON, getByLabelText} = renderScreenForTest(ScanFriendCodeScreen, createEmptyTestStore());
    // wait for cancel to render (indicates that permission is granted)
    const cancel = await waitForElement(() => getByLabelText("Abbrechen"));
    expect(asJSON()).toMatchSnapshot();
  });

  it(`cancel button works`, async () => {
    permissionGranted = true;
    const {asJSON, getByLabelText} = renderScreenForTest(ScanFriendCodeScreen, createEmptyTestStore());
    const cancel = await waitForElement(() => getByLabelText("Abbrechen"));
    // TODO Does not work with the current test library at the moment. The fireEvent -> press call is not working.
    // fireEvent.press(cancel);
    // expect(Navigation.goBack.mock.calls.length).toBe(1);
  });

  it(`shows permission denied`, async () => {
    permissionGranted = false;
    const {asJSON, getByTestId} = renderScreenForTest(ScanFriendCodeScreen, createEmptyTestStore());
    await waitForElement(() => getByTestId("permission-denied-info"));
    expect(asJSON()).toMatchSnapshot();
  });
});