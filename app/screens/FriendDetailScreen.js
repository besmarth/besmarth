import React from "react";
import {FlatList, StyleSheet, Text, View} from "react-native";
import {bindActionCreators} from "redux";
import {loadFriendChallenges} from "../services/Challenges";
import {connect} from "react-redux";
import {FriendChallengeProgress} from "../components/Challenge/FriendChallengeProgress";
import {ActivityIndicator, Card} from "react-native-paper";

class FriendDetailScreen extends React.Component {

  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    this.reload().then();
  }

  async reload() {
    this.setState({loading: true});
    try {
      const friend = this.props.route?.params?.friend;
      this.props.navigation.setOptions({
        title: friend?.username
      });
      const friend_id = friend.id;
      const challenges = await loadFriendChallenges(friend_id);
      this.setState({
        ...this.state,
        loading: false,
        friend,
        challenges
      });
    } catch (e) {
      this.setState({
        ...this.state,
        loading: false,
        error: "Challenges konnten nicht geladen werden (" + JSON.stringify(e) + ")"
      });
      console.log(e);
    }
  }

  render() {
    if (this.state.error) {
      return (
        <View testID="view" style={styles.container}>
          <Text>{this.state.error}</Text>
        </View>
      );
    }
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator animating={true} size={100}/>
        </View>
      );
    }
    const challenges = this.state.challenges;
    const friend = this.state.friend;
    if (challenges.length > 0) {
      return (
        <View testID="view" style={styles.container}>
          {/*<Headline style={[styles.heading, {marginLeft: 10}]}>Geteilte Challenges</Headline>*/}
          <FlatList
            data={challenges}
            refreshing={this.state.loading}
            onRefresh={this.reload.bind(this)}
            renderItem={({item}) => <Card style={styles.challengeCard}><FriendChallengeProgress participation={item}
                                                                                                challenge={item.challenge}></FriendChallengeProgress></Card>}
            keyExtractor={item => item.id + ""}
          />
        </View>
      );
    } else {
      return (
        <View testID="view"
          style={[styles.container, {padding: 10, display: "flex", justifyContent: "center", alignItems: "center"}]}>
          <Text>{friend.username} hat keine Challenges geteilt</Text>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  heading: {
    fontSize: 20,
  },
  challengeCard: {
    margin: 8,
    backgroundColor: "#ddd"
  }
});

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FriendDetailScreen);
