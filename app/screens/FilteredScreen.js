import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {Caption, Surface, TouchableRipple} from 'react-native-paper';
import {TagSelect} from "../components/Tag-Select";
import {fetchFilteredChallenges} from "../services/Challenges";
import Navigation from "../services/Navigation";
import {notNull} from "../services/utils";
import * as Colors from "../styles/colors";

class FilteredScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        }
    }

    componentDidMount() {
        this.reload().then();
    }

    async reload() {
        this.setState({loading: true});

        const allTags = [
            {id: 1, label: 'Energie', german: 'Energie'},
            {id: 2, label: 'Transport', german: 'Transport'},
            {id: 3, label: 'Abfall', german: 'Abfall'},
            {id: 4, label: 'Konsum', german: 'Konsum'},
            {id: 101, label: 'Daily', german: 'Täglich'},
            {id: 102, label: 'Weekly', german: 'Wöchentlich'},
            {id: 103, label: 'Monthly', german: 'Monatlich'},
            {id: 201, label: 'Easy', german: 'Einfach'},
            {id: 202, label: 'Advanced', german: 'Mittel'},
            {id: 203, label: 'Hard', german: 'Schwer'},
        ];

        try {
            /* get all challenges, when no filtering is applied (no query params) */
            let filteredChallenges = await fetchFilteredChallenges([], [], []);

            this.setState({
                filters: [],
                allTag: allTags,
                loading: false,
                filteredChallenges,
            });

        } catch (e) {
            console.log(e);
            this.setState({
                error: JSON.stringify(e),
                loading: false
            });
        }
    }


    render() {

        return (
            <View style={{flex: 1}}>
                <View style={{marginHorizontal: 10}}>
                    <Text style={styles.mainTitle}>Gefilterte Challenges</Text>
                    <TagSelect
                        data={this.state.filters}
                        ref={(tag) => {
                            this.tag = tag;
                        }}
                        labelAttr={'german'}
                        itemStyle={styles.itemPrev}
                        itemLabelStyle={styles.labelPrev}
                        onItemPress={() => {
                            this.removeTag(this.tag.itemsSelected)
                            this.tag.resetSelected();

                        }}
                        testID="tags"
                    />
                    <Text style={styles.labelText}>Kriterien auswählen</Text>
                    <TagSelect
                        data={this.state.allTag}
                        ref={(rec) => {
                            this.rec = rec;
                        }}
                        labelAttr={'german'}
                        itemStyle={styles.item}
                        itemLabelStyle={styles.label}
                        onItemPress={() => {
                            this.addTag(this.rec.itemsSelected);
                            this.rec.resetSelected();
                        }}
                    />
                </View>

                <FlatList
                    numColumns={2}
                    testID="flatlist"
                    persistentScrollbar={true}
                    style={styles.participationList}
                    data={this.state.filteredChallenges}
                    ListEmptyComponent={<Caption style={styles.emptyInfo}>
                        Keine Challenges zu den Filterkriterien gefunden...</Caption>}
                    onRefresh={this.reload.bind(this)}
                    refreshing={this.state.loading}
                    keyExtractor={(item) => item.id.toString()}
                    //ChallengeItem is name of function. Takes two parameters: challenge + onPressItem
                    renderItem={({item}) => <ChallengeItem challenge={item}
                                                           onPressItem={this.onOpenChallenge}/>}
                />
            </View>
        )

    }

    removeTag(chosenTag) {
        this.setState({filters: this.state.filters.filter(item => !chosenTag.includes(item))});
        for (let i = 0; i < chosenTag.length; i++) {
            if (!this.state.allTag.includes(chosenTag[i])) {
                this.setState({
                    allTag: [
                        ...this.state.allTag,
                        chosenTag[i]
                    ]
                }, () => {
                        this.recallFilterChallenges()
                    }
                    )
            }
        }



    }

    addTag(chosenTag) {
        this.setState({allTag: this.state.allTag.filter(item => !chosenTag.includes(item))});
        for (let i = 0; i < chosenTag.length; i++) {
            if (!this.state.filters.includes(chosenTag[i])) {
                this.setState({
                        filters: [
                            ...this.state.filters,
                            chosenTag[i]
                        ]
                    }, () => {
                        this.recallFilterChallenges()
                    }
                )
            }
        }
    }

    // Updates the shown challenges
    async recallFilterChallenges() {
        let topics = [];
        let periodicity = [];
        let difficulty = [];
        for (let i = 0; i < this.state.filters.length; i++) {
            if (this.state.filters[i].id < 100) {
                topics.push(this.state.filters[i].label)
            }
            if (this.state.filters[i].id < 200 && this.state.filters[i].id > 100) {
                periodicity.push(this.state.filters[i].label.toUpperCase())
            }
            if (this.state.filters[i].id > 200) {
                difficulty.push(this.state.filters[i].label.toUpperCase())
            }
        }

        let filteredChallenge = await fetchFilteredChallenges(topics, periodicity, difficulty);
        this.setState({filteredChallenges: filteredChallenge});
    }

    async onOpenChallenge(challengeItem) {
        Navigation.push("MyChallengesTab", "ChallengeDetail", {challenge: challengeItem});
        //see push() -> ../services/Navigation
    }

}


function ChallengeItem({challenge, onPressItem}) {

    const image = notNull(challenge.image) ? {uri: challenge.image} : require("../assets/images/no-challenge-image.png");

    return (
        <View style={{width: '50%'}}>
            <TouchableRipple
            // call onOpenChallenge function
            onPress={() => onPressItem(challenge)}
            testID="touch-ripple"
            >
                <Surface style={styles.participationItem}>
                    <View style={styles.itemContent}>
                        <Image source={image} style={styles.cardImageStyle}/>
                        <View style={styles.cardBottomBarContainer}>
                            <Text numberOfLines={1} style={styles.cardTitle}>{challenge.title} </Text>
                            <Text style={styles.cardRating}>{challenge.rating > 0 ?  "☆" +challenge.rating : "☆  -"}</Text>
                        </View>
                    </View>
                </Surface>
            </TouchableRipple>
        </View>

    )
}

const styles = StyleSheet.create({
    mainTitle: {
        fontWeight: '300',
        fontSize: 30,
        textAlign: "left",
        paddingVertical: 8,
    },

    participationList: {
        marginHorizontal: 5,
    },
    participationItem: {
        flex: 1,
        height: 160,
        borderRadius: 15,
        margin: 5,
    },

    itemContent: {
        alignItems: "center",
        flexDirection: "column",
        flex: 1,
    },
    cardImageStyle: {
        width: '100%',
        height: '100%',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        flex: 4,
        borderWidth: 2.5,
        borderColor: Colors.green,
    },
    cardBottomBarContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 6,
        paddingHorizontal: 8,
        backgroundColor: Colors.green,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
    },
    cardTitle: {
        flex: 3,
        fontSize: 14,
        color: 'white',
    },
    cardRating: {
        flex: 0.9,
        fontSize: 14,
        color: 'white',
    },

    labelText: {
        color: '#333',
        fontSize: 18,
        fontWeight: '300',
        paddingBottom: 5,
        paddingLeft: 4
    },
    item: {
        borderWidth: 1,
        borderColor: Colors.green,
        backgroundColor: '#FFF',
    },
    label: {
        color: '#333'
    },

    itemPrev: {
        borderWidth: 1,
        borderColor: Colors.green,
        backgroundColor: Colors.green,
        opacity: 0.8
    },
    labelPrev: {
        color: '#ffffff',
        fontWeight: '500'
    },

    emptyInfo: {
    marginTop: 60,
    paddingHorizontal: 8,
    textAlign: "center"
    },

    cardCoverStyle: {
        width: 85,
        height: 85,
    },



});


const mapStateToProps = state => {
    return {};
};
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FilteredScreen);
