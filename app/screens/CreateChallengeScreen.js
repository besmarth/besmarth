import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Alert, Image, Picker, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native'
import Form from 'react-native-advanced-forms'
import SwitchSelector from "react-native-switch-selector";
import * as ImagePicker from "expo-image-picker";
import {postNewChallenge} from "../services/Challenges";
import {Button, IconButton} from "react-native-paper";
import {Tooltip} from "react-native-elements";
import Navigation from "../services/Navigation";
import Toast from "react-native-toast-message";
import theme from "../themes/theme";


class CreateChallengeScreen extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            title: null,
            duration: null,
            description: null,
            frequency: 'DAILY',
            difficulty: 'EASY',
            topic: 3,
            tags: [],
            image: 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/17812843/logo_square.png',
            challenge: null
        };
    }

    componentDidMount() {
        this.reload().then();
    }


    async reload() {
        this.setState({loading: true});
        try {
            this.setState({
                loading: false
            });
        } catch (e) {
            console.log(e);
            this.setState({
                error: JSON.stringify(e),
                loading: false
            });
        }
    }

    render() {
        let {
            title, duration,
            description
        } = this.state;

        const pickImage = async () => {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [4, 3],
                quality: 1,
            });

            if (!result.cancelled) {
                this.setState({image: result.uri})
            }
        };


        return (

            <View style={styles.container}>
                <ScrollView ref={e => {
                    this.scrollView = e;
                }}>
                    <Form ref={this._onFormRef} onChange={this.onChange} onSubmit={this.onSubmit}
                          validate={this.validate}>
                        <Form.Layout style={styles.row}>
                            <Form.Layout style={styles.columns}>
                                <Form.Field name="title" label="Challenge Name"
                                            labelTextStyle={styles.formLabelTitle}
                                            labelRightContent={
                                                <Tooltip width={200} height={100} backgroundColor='#7b9c93'
                                                         popover={<Text>Geben Sie der Challenge einen prägnanten
                                                             Namen.</Text>}>
                                                    <IconButton small
                                                                icon="information-outline"
                                                                color={styles.iconInfoButton.color}
                                                    />
                                                </Tooltip>
                                            }>
                                    <Form.TextField accessibilityLabel="Challenge Name" value={title}/>
                                </Form.Field>
                            </Form.Layout>
                        </Form.Layout>
                        <Form.Layout style={styles.row}>
                            <Form.Layout style={styles.columns}>
                                <Form.Field name="description" label="Challenge Beschreibung"
                                            labelTextStyle={styles.formLabelTitle} labelRightContent={
                                    <Tooltip width={200} height={110} backgroundColor='#7b9c93'
                                             popover={<Text>Geben Sie der Challenge eine klare und passende
                                                 Beschreibung.</Text>}>
                                        <IconButton small
                                                    icon="information-outline"
                                                    color={styles.iconInfoButton.color}
                                        />
                                    </Tooltip>
                                }>
                                    <Form.TextField accessibilityLabel="Challenge Description" value={description}/>
                                </Form.Field>
                            </Form.Layout>
                        </Form.Layout>
                        <Form.Layout style={styles.row}>
                            <Form.Field name="duration" label="Dauer" labelTextStyle={styles.formLabelTitle}
                                        labelRightContent={
                                            <Tooltip width={250} height={150} backgroundColor='#7b9c93'
                                                     popover={<Text>Geben Sie die gewünschte Dauer an. Die Dauer
                                                         misst
                                                         sich stets an der Häufigkeit. Z.B. bei der Auswahl "Daily"
                                                         in
                                                         Anzahl Tagen. Bei "Monthly" als die Anzahl Monate.</Text>}>
                                                <IconButton small
                                                            icon="information-outline"
                                                            color={styles.iconInfoButton.color}
                                                />
                                            </Tooltip>
                                        }>
                                <Form.TextField accessibilityLabel="Challenge Duration" value={duration}
                                                keyboardType='numeric'/>
                            </Form.Field>
                        </Form.Layout>
                        <Form.Layout style={styles.row}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text style={styles.labelTitle}>Häufigkeit</Text>
                                <Tooltip width={250} height={90} backgroundColor='#7b9c93'
                                         popover={<Text>Geben Sie an, wie oft die Challenge gemacht werden
                                             muss.</Text>}>
                                    <IconButton small
                                                icon="information-outline"
                                                color={styles.iconInfoButton.color}
                                    />
                                </Tooltip>
                            </View>
                            <SwitchSelector
                                initial={0}
                                textColor='#000000'
                                selectedColor='#FFFFFF'
                                buttonColor={theme.colors.primary}
                                borderColor={theme.colors.primary}
                                hasPadding
                                options={[
                                    {label: "Täglich", value: "DAILY"},
                                    {label: "Wöchentlich", value: "WEEKLY"},
                                    {label: "Monatlich", value: "MONTHLY"}
                                ]}
                                onPress={value => this.setState({frequency: value})}
                            />
                        </Form.Layout>
                        <Form.Layout style={styles.row}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text style={styles.labelTitle}>Schwierigkeit</Text>
                                <Tooltip width={250} height={90} backgroundColor='#7b9c93'
                                         popover={<Text>Geben Sie ein passendes Schwierigkeitslevel für die
                                             Challenge
                                             an.</Text>}>
                                    <IconButton small
                                                icon="information-outline"
                                                color={styles.iconInfoButton.color}
                                    />
                                </Tooltip>
                            </View>
                            <SwitchSelector
                                initial={0}
                                textColor='#000000'
                                selectedColor='#FFFFFF'
                                buttonColor={theme.colors.primary}
                                borderColor={theme.colors.primary}
                                hasPadding
                                options={[
                                    {label: "Einfach", value: "EASY"},
                                    {label: "Mittel", value: "MEDIUM"},
                                    {label: "Schwer", value: "HARD"}
                                ]}
                                onPress={value => this.setState({difficulty: value})}
                            />
                        </Form.Layout>
                        <Form.Layout style={styles.row}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text style={styles.labelTitle}>Thema</Text>
                                <Tooltip width={250} height={140} backgroundColor='#7b9c93'
                                         popover={<Text>Wählen Sie das passende Thema zur Challenge. Diese Angabe
                                             ist
                                             relevant, damit die Challenge später kategorisiert gefunden werden
                                             kann.</Text>}>
                                    <IconButton small
                                                icon="information-outline"
                                                color={styles.iconInfoButton.color}
                                    />
                                </Tooltip>
                            </View>
                            <Picker selectedValue={this.state.topic} onValueChange={(value) => {
                                this.setState({topic: value})
                            }}>
                                <Picker.Item label="Energie" value="0"/>
                                <Picker.Item label="Abfall" value="1"/>
                                <Picker.Item label="Transport" value="2"/>
                                <Picker.Item label="Konsum" value="3"/>


                            </Picker>
                        </Form.Layout>
                        {/*                            <Form.Layout style={styles.row}>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <Text style={styles.labelTitle}>Tags</Text>
                                    <Tooltip width={250} height={145} backgroundColor='#7b9c93'
                                             popover={<Text>Wählen Sie die passenden Tags zur Challenge. Diese Auswahl
                                                 ist
                                                 entscheidend, damit die Challenge später gefiltert gefunden werden
                                                 kann.</Text>}>
                                        <IconButton small
                                                    icon="information-outline"
                                                    color={styles.iconInfoButton.color}
                                        />
                                    </Tooltip>
                                </View>
                                <TagSelect
                                    data={allTags}
                                    itemStyle={styles.item}
                                    itemLabelStyle={styles.label}
                                    itemStyleSelected={styles.itemSelected}
                                    itemLabelStyleSelected={styles.labelSelected}
                                    onItemPress={(item) => {
                                        if (this.state.tags.includes(item.label)) {
                                            const index = this.state.tags.indexOf(item.label);
                                            if (index > -1) {
                                                this.state.tags.splice(index, 1);
                                            }
                                        } else {
                                            this.state.tags.push(item.label)
                                        }
                                    }}
                                />
                            </Form.Layout>*/}
                    </Form>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={styles.labelTitle}>Challenge Bild</Text>
                        <Tooltip width={250} height={150} backgroundColor='#7b9c93'
                                 popover={<Text>Wählen Sie aus Ihrer Galerie das passende Bild für die Challenge
                                     aus.
                                     Falls Sie kein eigenes Bild hochladen, wird das angezeigte Standardbild
                                     verwendet.</Text>}>
                            <IconButton small
                                        icon="information-outline"
                                        color={styles.iconInfoButton.color}
                            />
                        </Tooltip>
                    </View>
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginBottom: 5
                    }}>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <Image
                                source={{
                                    uri: this.state.image
                                }}
                                style={styles.imagePreview}
                            />
                            <TouchableOpacity style={styles.importButton}
                                              onPress={pickImage}
                            >
                                <Text>Eigenes Bild hochladen</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Button style={{marginTop: 10}} mode="contained"
                            onPress={() => this.form.validateAndSubmit()}
                            testID="create-button">Challenge erstellen</Button>
                </ScrollView>
            </View>
        )
    }

    _onFormRef = e => {
        this.form = e
    };
    onChange = (values) => {
        this.setState(values)
    };
    onSubmit = (values) => {

        postNewChallenge(this.state).then((reponse) => {
            Toast.show({
                text1: "Deine Challenge wurde erstellt",
                text2: "Aus Sicherheitsgründen wird deine Challenge vor der Freigabe durch uns geprüft!",
                position: 'bottom', type: 'success', visibilityTime: 4000
            });
            this.setState({title: null});
            this.setState({description: null});
            this.setState({duration: null});
            this.setState({frequency: 'DAILY'});
            this.setState({difficulty: 'EASY'});
            this.setState({topic: 3});
            this.setState({tags: []});
            this.setState({image: 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/17812843/logo_square.png'});
            Navigation.push("MyChallengesTab", "MyChallenges", {})
        }).catch((error) => {
            Toast.show({
                text1: error.toString().substring(7, error.toString().length),
                position: 'bottom', type: 'error', visibilityTime: 4000
            })
        })
    };
    validate = (values) => {
        let error = false;
        const ret = Object.keys(this.state).reduce((m, v) => {
                if (v === 'title' || v === 'description' || v === 'duration') {
                    if (!values[v] || !values[v].length) {
                        m[v] = Form.VALIDATION_RESULT.MISSING;
                        error = true
                    }
                }
                return m
            }
            , {}
        );
        if (error) {
            Alert.alert('Füllen Sie bitte alle notwendigen Felder korrekt aus!')
        }
        return ret
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10
    },
    formLabelTitle: {
        marginTop: 5,
        fontWeight: 'bold',
        fontSize: 18
    },
    labelTitle: {
        marginLeft: 5,
        marginTop: 15,
        fontWeight: 'bold',
        fontSize: 18
    },
    labelText: {
        color: '#333',
        fontSize: 18,
        fontWeight: '500',
    },
    item: {
        borderWidth: 1,
        borderColor: '#008D36',
        backgroundColor: '#FFF',
    },
    iconInfoButton: {
        color: '#33796a',

    },
    label: {
        color: '#333'
    },
    itemSelected: {
        backgroundColor: '#008d36',
        borderColor: '#fff',
    },
    labelSelected: {
        color: '#FFF',
    },
    importButton: {
        backgroundColor: '#8FC9B9',
        height: 50,
        width: 150,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imagePreview: {
        borderRadius: 50,
        width: 100,
        height: 100,
        margin: 5
    },
    createButton: {
        backgroundColor: '#008534',
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },

});

const mapStateToProps = state => {
    return {};
};
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CreateChallengeScreen);
