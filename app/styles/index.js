import * as Colors from './colors'
import * as Typography from './typography'
import * as Spacing from './spacing'
import * as Buttons from './buttons'

export { Typography, Colors, Spacing, Buttons };
