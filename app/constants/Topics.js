export const topic1 = 'Energie';
export const topic2 = 'Transport';
export const topic3 = 'Abfall';
export const topic4 = 'Konsum';

/**
 * Returns the background color to a specific topic
 * @param name
 * @returns {string}
 */
export const getBackgroundColorFromTopic = name => {
    if (name === topic1) {
        return "#D7EAD9"
    } else if (name === topic2) {
        return "#B6DDE0"
    } else if (name === topic3) {
        return "#BEB1E0"
    } else if (name === topic4) {
        return "#F9BC8A"
    } else {
        return "#D7EAD9"
    }
}

/**
 * Returns the color to a specific topic
 * @param name
 * @returns {string}
 */
export const getColorFromTopic = name => {
    if (name === topic1) {
        return "#8ACB60"
    } else if (name === topic2) {
        return "#6684CB"
    } else if (name === topic3) {
        return "#988db3"
    } else if (name === topic4) {
        return "#c7966e"
    } else {
        return "#8ACB60"
    }
}

