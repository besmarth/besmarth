const primary = '#008534';

export default {
  primary,
  primaryText: 'white',
  tabIconDefault: '#ccc',
  tabIconSelected: primary,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: 'white',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: primary,
  noticeText: '#fff',
};
