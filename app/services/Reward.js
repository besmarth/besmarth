import RemoteStorage from "./RemoteStorage";
import LocalStorage from "./LocalStorage";
import {notNull} from "./utils";
import {isPhoneOnline} from "./phone-utils";
import RewardToastService from "./RewardToastService";

const REWARD_ACHIEVED_API_PATH = "/achievedReward";
const REWARD_CACHE_KEY = "achievedReward";

const REWARD_API_PATH = "/reward/";

/**
 * Fetches all rewards from the server
 */
export async function fetchAchievedRewards() {
  if (await isPhoneOnline()) {
    const response = await RemoteStorage.get(REWARD_ACHIEVED_API_PATH);
    const rewards = response.data;
    await LocalStorage.setCacheItem(REWARD_CACHE_KEY, rewards);
    return rewards;
  } else {
    console.log("Device is offline, getting rewards from cache");
    const cachedRewards = await LocalStorage.getCacheItem(REWARD_CACHE_KEY) || [];
    if (notNull(cachedRewards)) {
      return cachedRewards;
    } else {
      throw new Error("device is offline and no rewards are cached");
    }
  }
}


/**
 * Fetches all rewards from the server
 */
export async function fetchReward(id) {
  if (await isPhoneOnline()) {
    const response = await RemoteStorage.get(REWARD_API_PATH + id);
    return response.data;
  } else {
    console.log("Device is offline, not loading them");
  }
}

export async function loadRewards(rewardIds) {
  try {
    let rewards = [];
    if (notNull(rewardIds) && rewardIds.length > 0) {
      for (let i = 0; i < rewardIds.length; i++) {
        const reward = await fetchReward(rewardIds[i].trim());
        rewards.push(reward);
      }
      RewardToastService.showRewards(rewards);
    }

  } catch (e) {
    // TODO error handling
    console.log(e);
  }
}

export function checkHeader(headers) {
  if (notNull(headers["x-achieved-reward"])) {
    const achievedRewards = headers["x-achieved-reward"].replace("[", "").replace("]", "").trim().split(",");
    loadRewards(achievedRewards).then();
  }
}

