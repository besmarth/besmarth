import {initAuth} from "../Auth";
import * as SecureStore from "expo-secure-store";
import {ACCOUNT_CACHE_KEY} from "../AccountService";
import {createNetworkMock} from "../../test/test-utils";

const accountData = {
  username: "testusername",
  token: "mytoken",
  first_name: "Heinrich",
  email: "thatemail@web.de"
};

describe(`Auth`, () => {
  it(`mocked initAuth`, async () => {
    const mock = createNetworkMock();
    mock.reset();
    mock.onPost("/account", {pseudonymous: true}).reply(200, accountData);
    const store = {
      dispatch: jest.fn(),
      getState: jest.fn()
    };
    await SecureStore.setItemAsync(ACCOUNT_CACHE_KEY, null);
    const result = await initAuth()(store.dispatch, store.getState);
    expect(result).toBe(true);

    expect(store.dispatch.mock.calls.length).toBe(2);
    expect(store.dispatch.mock.calls[0][0]).toEqual({
      type: "ACCOUNT_UPDATE",
      account: accountData
    });
    expect(store.dispatch.mock.calls[1][0]).toEqual({
      type: "APP_LOADING_COMPLETE",
    });
    expect(mock.history.post.length).toBe(1);

  });
  it(`mocked initAuth from cache`, async () => {
    await SecureStore.setItemAsync(ACCOUNT_CACHE_KEY, JSON.stringify(accountData));
    const store = {
      dispatch: jest.fn(),
      getState: jest.fn()
    };

    await initAuth()(store.dispatch, store.getState);

    expect(store.dispatch.mock.calls.length).toBe(2);
    expect(store.dispatch.mock.calls[0][0]).toEqual({
      type: "ACCOUNT_UPDATE",
      account: accountData
    });
    expect(store.dispatch.mock.calls[1][0]).toEqual({
      type: "APP_LOADING_COMPLETE",
    });
  });
});
