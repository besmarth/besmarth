
export function showToast(message, toastType='DEFAULT') {
  return (dispatch) => {
    dispatch({type: 'SHOW_TOAST', message, toastType});
  }
}

export function hideToast(message) {
  return (dispatch) => {
    dispatch({type: 'HIDE_TOAST'});
  }
}
