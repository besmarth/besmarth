/**
 * Returns true, if obj is not null or undefined
 */
export function notNull(obj) {
  return obj !== null && obj !== undefined;
}

export function emptyString(str) {
  return !notNull(str) || str.trim() === "";
}

export function isDevelopmentMode() {
  return __DEV__ && !window.__TESTMODE__;
}

export function isProductionMode() {
  return !__DEV__ && !window.__TESTMODE__;
}

/**
 * Returns date with hours, minutes and seconds truncated.
 */
export function getTodayDate() {
  const now = new Date();
  now.setUTCHours(0);
  now.setUTCMinutes(0);
  now.setUTCSeconds(0);
  now.setUTCMilliseconds(0);
  return now;
}

/**
 * Returns obj if not null, if obj == null, returns replacement
 */
export function nvl(obj, replacement) {
  return notNull(obj) ? obj : replacement;
}

export function getDateTime() {
  return new Date();
}

/**
 * Convert an array to a map, key is extracted from each list element
 * @param list
 * @param keyExtractor
 */
export function toMap(list, keyExtractor) {
  if (!notNull(list) || !notNull(keyExtractor)) {
    throw new Error("list and keyextractor cannot be null!");
  }
  return list.reduce((map, elem) => {
    map[keyExtractor(elem)] = elem;
    return map;
  }, {});
}

/**
 * True, if date is today
 */
export function isToday(date) {
  if (notNull(date)) {
    date.setUTCHours(0);
    date.setUTCMinutes(0);
    date.setUTCSeconds(0);
    date.setUTCMilliseconds(0);
    return datesEqual(getTodayDate(), date);
  } else {
    return false;
  }
}

/**
 * Formats a date according to the internal representation (ISO)
 */
export function dateToString(date) {
  return date.toISOString();
}

/**
 * Parses the date (in ISO string, like toISOString()) and returns a javascript date
 */
export function parseDate(isoDateString) {
  return (new Date(isoDateString));
}

/**
 * Returns true, if and only if the two dates are equal
 */
export function datesEqual(date1, date2) {
  return date1 === date2 || (notNull(date1) && notNull(date2) && date1.getTime() === date2.getTime());
}