import LocalStorage from "./LocalStorage";
import {notNull} from "./utils";
import {isPhoneOnline} from "./phone-utils";
import RemoteStorage from "./RemoteStorage"

const CHALLENGES_API_PATH = "/challenges/topic/";
export const CHALLENGES_CACHE_KEY = "challenges";

function buildChallengeParticipationPath(challengeId) {
    return "/challenges/" + challengeId + "/participation/";
}

function buildChallengeParticipationProgressPath(challengeId) {
    return "/challenges/" + challengeId + "/participation/progress/";
}

/**
 * Fetches all challenges of a topic from the server
 */
export async function fetchChallenges(topicId) {
    if (await isPhoneOnline()) {
        const response = await RemoteStorage.get(CHALLENGES_API_PATH + topicId + "/");
        const challenges = response.data;
        await LocalStorage.setCacheItem(CHALLENGES_CACHE_KEY, challenges);
        return challenges;
    } else {
        console.log("Device is offline, getting challenges from cache");
        const cachedChallenges = await LocalStorage.getCacheItem(CHALLENGES_CACHE_KEY);
        if (notNull(cachedChallenges)) {
            return cachedChallenges.filter(c => c.topic === topicId);
        } else {
            throw new Error("device is offline and no challenges are cached");
        }
    }
}

/**
 * Fetches a challenge by its challenge id
 */
export async function fetchChallenge(challengeId) {
    if (!await isPhoneOnline()) {
        throw new Error("no internet connection available");
    }
    const response = await RemoteStorage.get("/challenges/" + challengeId + "/");
    const challenge = response.data;
    return challenge;
}

/**
 * Loads the progress of a single challenge into
 */
export async function fetchChallengeParticipation(challenge) {
    if (!await isPhoneOnline()) {
        throw new Error("no internet connection available");
    }

    try {
        const response = await RemoteStorage.get(buildChallengeParticipationPath(challenge.id));
        return response.data;
    } catch (e) {
        // 404 happens when the user did not already signed up for the challenge
        if (e.status !== 404) {
            throw new Error("Unerwarteter Fehler beim Laden", e);
        } else {
            return null;
        }
    }
}

/**
 * Signs the user up to that challenge
 * @param challenge
 * @returns {Function}
 */
export async function signUpChallenge(challenge, duration, periodicity) {

    let data = new FormData();
    data.append("duration", duration);
    data.append("periodicity", periodicity);

    const response = await RemoteStorage.post(buildChallengeParticipationPath(challenge.id), data);
    return response.data;
}

export async function unfollowChallenge(challenge) {
    await RemoteStorage.delete(buildChallengeParticipationPath(challenge.id));
}

/**
 * Marks the challenge as 'done' for one time unit (day/week/month)
 */
export async function createChallengeProgress(challenge) {
    const response = await RemoteStorage.post(buildChallengeParticipationProgressPath(challenge.id));
    return response.data;
}

/**
 * Shares the challenge to the user's friends
 * @param challenge
 */
export async function changeSharedStateChallenge(challenge, shared) {
    const response = await RemoteStorage.patch(buildChallengeParticipationPath(challenge.id), {shared});
    return response.data;
}

/**
 * Load challenges of a friend.
 * Returns an array of participations
 */
export async function loadFriendChallenges(friend_id) {
    const response = await RemoteStorage.get(`/account/friends/${friend_id}/challenges`);
    return response.data;
}

/**
 * Loads all challenges the user is currently taking part
 * @returns {Promise<unknown>}
 */
export async function fetchOwnChallengeParticipations() {
    const response = await RemoteStorage.get(`/account/participations`);
    return response.data;
}

/**
 * The log progress button is only visible if it was not already pressed the same day/week/month
 */

export function progressButtonAvailable(participation) {
    if (!notNull(participation?.progress) || participation.progress.length === 0) {
        console.log("participation:" + participation); //TODO
        return true;
    }
    return participation.progress_loggable;
}

export function getNumberOfCompletedChallenges(participation, challenge) {
    return Math.floor(((participation?.progress?.length || 0) / challenge.duration));
}

export async function postNewChallenge(challenge) {

    if (!await isPhoneOnline()) {
        throw new Error("Keine Internet Verbindung vorhanden");
    }

    let formdata = new FormData();
    formdata.append("title", challenge.title);
    formdata.append("description", challenge.description);
    formdata.append("image", {uri: challenge.image, name: 'image.jpg', type: 'image/jpeg'});
    formdata.append("duration", challenge.duration);
    formdata.append("color", '#0b85b5');
    formdata.append("difficulty", challenge.difficulty);
    formdata.append("periodicity", challenge.frequency);
    formdata.append("topic", challenge.topic);
    formdata.append("category", 'MOBILITY');

    const response = await RemoteStorage.post('/newChallenges/add/', formdata);
    if (response.status !== 201) {
        throw new Error("Challenge konnte nicht erstellt werden.");
    }
    return response.data.status
}

export async function fetchFilteredChallenges(topic, periodicity, difficulty) {
    console.log('function entered with value: ' + topic[0] + difficulty[0]);

    let query_topic = '';
    let query_periodicity = '';
    let query_dif = '';  // difficulty=<var>  HARD EASY

    if (topic[0] !== undefined) {
        query_topic = '&topic=' + topic.toString()
    }
    if (periodicity[0] !== undefined) {
        query_periodicity = '&periodicity=' + periodicity.toString()
    }
    if (difficulty[0] !== undefined) {
        query_dif = '&difficulty=' + difficulty.toString()
    }

    console.log('filterchallenges/?' + query_topic + query_periodicity + query_dif);

    const response = await RemoteStorage.get('filterchallenges/?' + query_topic + query_periodicity + query_dif);
    if (response.status == 400) {
        throw new Error("Parameter existiert nicht");
    } else {
        const challenges = response.data;
        return challenges;
    }
}

//Write rating in the Database. A second rating for the same challenge by the same user is overwritten.
export async function postChallengeRating(challengeID, rating, comment) {

    const json = JSON.stringify({
        "challenge_id": challengeID,
        "starrating": rating,
        "comment": comment
    });
    const response = await RemoteStorage.post('/challenge/rating/', json);
    return response.status;
}

