import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import HomeScreen from "../screens/Home/HomeScreen";
import DailyTipScreen from "../screens/DailyTipScreen";
import TopicScreen from "../screens/TopicScreen";
import ChallengeDetailScreen from "../screens/ChallengeDetailScreen";
import CustomizeChallengeScreen from "../screens/CustomizeChallengeScreen"
import MyChallengesScreen from "../screens/MyChallengesScreen";
import CreateChallengeScreen from "../screens/CreateChallengeScreen";
import FriendsScreen from "../screens/Friends/FriendsScreen";
import FriendDetailScreen from "../screens/FriendDetailScreen";
import ManageAccountScreen from "../screens/Account/ManageAccountScreen";
import SignInScreen from "../screens/Account/SignInScreen";
import SignUpScreen from "../screens/Account/SignUpScreen";
import AccountCodeScreen from "../screens/Friends/AccountCodeScreen";
import ScanFriendCodeScreen from "../screens/Friends/ScanFriendCodeScreen";
import RewardScreen from "../screens/RewardScreen";
import FilteredScreen from "../screens/FilteredScreen";
import {Image} from "react-native";

const HomeStack = createStackNavigator();

function LogoTitle() {
    return (
        <Image
            style={{width: 50, height: 50}}
            source={{
                uri: 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/17812843/logo_square.png'
            }}
        />
    );
}

export function HomeStackScreen() {
    return (
        <HomeStack.Navigator initialRouteName="Home">
            <HomeStack.Screen name="Home" component={HomeScreen}
                              options={{headerTitle: props => <LogoTitle {...props} />}}/>
            <HomeStack.Screen name="DailyTip" component={DailyTipScreen}
                              options={{headerShown: false, title: "Tipp des Tages"}}/>
            <HomeStack.Screen name="TopicDetail" component={TopicScreen}/>
            <HomeStack.Screen name="ChallengeDetail" component={ChallengeDetailScreen}/>
        </HomeStack.Navigator>
    );
}

const MyChallengesStack = createStackNavigator();

export function MyChallengesStackScreen() {
    return (
        <MyChallengesStack.Navigator initialRouteName="MyChallenges">
            <MyChallengesStack.Screen name="MyChallenges" component={MyChallengesScreen}
                                      options={{headerTitle: props => <LogoTitle {...props} />}}/>
            <MyChallengesStack.Screen name="ChallengeDetail" component={ChallengeDetailScreen}
                                      options={{title: "Challenge"}}/>
            <MyChallengesStack.Screen name="Filtered" component={FilteredScreen}
                                      options={{headerTitle: props => <LogoTitle {...props} />}}/>
            <MyChallengesStack.Screen name="CustomizeChallenge" component={CustomizeChallengeScreen}
                                      options={{title: "Challenge bearbeiten"}}/>
            <MyChallengesStack.Screen name="CreateChallenge" component={CreateChallengeScreen}
                                      options={{title: "Challenge erstellen"}}/>
        </MyChallengesStack.Navigator>
    );
}

const FriendsStack = createStackNavigator();

export function FriendsStackScreen() {
    return (
        <FriendsStack.Navigator initialRouteName="Friends">
            <FriendsStack.Screen name="Friends" component={FriendsScreen}
                                  options={{headerTitle: props => <LogoTitle {...props} />}}/>
            <FriendsStack.Screen name="FriendDetail" component={FriendDetailScreen} options={{title: "Freund"}}/>
            <FriendsStack.Screen name="AccountCode" component={AccountCodeScreen} options={{title: "Dein Code"}}/>
            <FriendsStack.Screen name="ScanFriendCode" component={ScanFriendCodeScreen}
                                 options={{title: "Code scannen"}}/>
            <FriendsStack.Screen name="ChallengeDetail" component={ChallengeDetailScreen}
                                 options={{title: "Challenge"}}/>
        </FriendsStack.Navigator>
    );
}

const AccountStack = createStackNavigator();

export function AccountStackScreen() {
    return (
        <AccountStack.Navigator initialRouteName="ManageAccount">
            <AccountStack.Screen name="ManageAccount" component={ManageAccountScreen}
                                  options={{headerTitle: props => <LogoTitle {...props} />}}/>
            <AccountStack.Screen name="SignIn" component={SignInScreen} options={{title: "Sign In"}}/>
            <AccountStack.Screen name="SignUp" component={SignUpScreen} options={{title: "Sign Up"}}/>
            <AccountStack.Screen name="Rewards" component={RewardScreen} options={{title: "Rewards"}}/>
        </AccountStack.Navigator>
    );
}