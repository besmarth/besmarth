import React from "react";
import {createEmptyTestStore, createNetworkMock, renderForTest} from "../../test/test-utils";
import TopicLevel from "../TopicLevel";
import {waitForElement} from "@testing-library/react-native";

const mock = createNetworkMock();

const topic3 = {
  "imageUrl": "/media/images/topic-levels/level01.png",
  "level": 1,
  "levelDescription": "Klimaneuling",
  "score": 0,
  "scoreNextLevel": 1,
  "scorePrevLevel": 0,
};

const topic4 = {
  "imageUrl": "/media/images/topic-levels/level02.png",
  "level": 2,
  "levelDescription": "Klimasupporter",
  "score": 0,
  "scoreNextLevel": 5,
  "scorePrevLevel": 1,
};

describe("TopicLevelTest", () => {
  it(`without progress bar`, async () => {
    mock.reset();
    mock.onGet("/topics/4/progress").reply(200, [topic4]);

    const store = createEmptyTestStore();

    const {getByTestId, asJSON} = renderForTest(<TopicLevel topicId="4" size="70" textColor="white"/>, store);

    await waitForElement(() => getByTestId("view"));
    expect(asJSON()).toMatchSnapshot();
  });

  it(`with progress bar`, async () => {
    mock.reset();
    mock.onGet("/topics/3/progress").reply(200, [topic3]);

    const store = createEmptyTestStore();

    const {getByTestId, asJSON} = renderForTest(<TopicLevel topicId="3" size="120" textColor="black" showProgressBar/>, store);

    await waitForElement(() => getByTestId("view"));
    expect(asJSON()).toMatchSnapshot();
  });
});