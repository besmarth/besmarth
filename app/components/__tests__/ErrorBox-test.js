import React from "react";
import ErrorBox from "../ErrorBox";
import {renderForSnapshot} from "../../test/test-utils";

describe("ErrorBox", () => {
  it(`renders`, () => {
    const tree = renderForSnapshot(<ErrorBox
      errors={{"mail": ["The mail is invalid"], "others": ["Something else is fishy"]}}/>).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it(`renders invisible`, () => {
    const tree = renderForSnapshot(<ErrorBox errors={null}/>).toJSON();
    expect(tree).toMatchSnapshot();
  });
});