import React from "react";
import {Text} from "react-native-paper";
import {isDevelopmentMode, notNull} from "../services/utils";
import Colors from "../constants/Colors";
import {View} from "react-native";

export default function ErrorBox(props) {
  if (notNull(props.errors)) {
    if (isDevelopmentMode()) {
      console.log("[DEV] ErrorBox is showing error", props.errors);
    }
    // algorithm in a nutshell:
    // check if javascript error, if display the message, else
    // check if only a string, if display it, else
    // iterate over all error groups
    //   iterate over all errors of the group
    //     print error
    // an error group could be the errors for a specific input field
    // if there is more than one error, the errors are prefixed
    // with a dash '-'
    let msg = "";
    try {
      if (notNull(props.errors.stack)) { // javascript error
        msg = props.errors.message;
      } else if (typeof props.errors === "string") {
        msg = props.errors;
      } else {
        let errCount = 0;
        for (let errorGroup in props.errors) {
          const group = props.errors[errorGroup];
          for (let i in group) {
            if (errCount > 0) {
              msg += "\n- ";
            }
            msg += group[i];
            errCount++;
          }
        }
        if (errCount > 1) {
          msg = "- " + msg;
        }
      }
    } catch (e) {
      // in case of error when formatting an error, just display error as json
      console.log(e);
      msg = JSON.stringify(props.errors);
    }
    return <View
      testID="Fehleranzeige"
      style={{backgroundColor: Colors.errorBackground, padding: 10}}>
      <Text style={{color: Colors.errorText}}>{msg}</Text>
    </View>;
  } else {
    return null;
  }
}
