import React from "react";
import {ScrollView, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {deleteAccount, updateAccount} from "../services/AccountService";
import {initAuth, signOut} from "../services/Auth";
import {connect} from "react-redux";
import Navigation from "../services/Navigation";
import {Button, Dialog, Paragraph, Portal, Switch, TextInput} from "react-native-paper";
import ErrorBox from "./ErrorBox";
import {emptyString} from "../services/utils";
import {showToast} from "../services/Toast";

/**
 * Form which enables the user to edit his account details
 */
class AccountForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      acceptTermsOfService: false,
      pseudonymous: false
    };
  }

  componentDidMount() {
    if (!this.props.signUpMode) {
      this.setState({...this.state, ...this.props.account});
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.account.username !== prevProps.account.username) {
      if (!this.props.signUpMode) {
        this.setState({...this.props.account});
      }
    }
  }

  async onSubmit() {
    if (this.props.signUpMode && !this.state.acceptTermsOfService) {
      alert("Du musst den Geschäftsbedingungen zustimmen");
    } else if (this.props.signUpMode && emptyString(this.state.password)) {
      alert("Bitte gib ein Passwort ein");
    } else {
      try {
        await this.props.updateAccount(this.state);
        if (this.props.signUpMode) {
          this.props.showToast("Account erfolgreich angelegt");
        } else {
          this.props.showToast("Daten erfolgreich gespeichert");
        }
        this.setState({...this.state, errors: null});
        Navigation.navigate("AccountTab","ManageAccount");
      } catch (e) {
        this.setState({
          ...this.state,
          errors: e
        });
      }
    }
  }

  onChangeField(fieldName, newValue) {
    this.setState({
      ...this.state,
      [fieldName]: newValue
    });
  }

  switchAccount() {
    Navigation.navigate("AccountTab","SignIn");
  }

  /**
   * deletes account and then creates anonymous account
   */
  async deleteAccount() {
    try {
      await this.props.deleteAccount(this.state);
      await signOut(this);
      await this.props.initAuth();
      this.props.showToast("Account erfolgreich gelöscht");
    } catch (e) {
      this.props.showToast("Account konnte nicht gelöscht werden, versuche es später noch einmal", "ERROR");
    }
  }

  _showDeleteDialog = () => this.setState({deleteDialogVisible: true});
  _hideDeleteDialog = () => this.setState({deleteDialogVisible: false});

  render() {
    let acceptTermsOfService;
    let loginWithAnotherAccount;
    let deleteAccountButton;

    if (this.props.signUpMode) {
      acceptTermsOfService = <View style={{...styles.formControl, ...styles.row}}>
        <Switch value={this.state.acceptTermsOfService}
                accessibilityLabel="Hiermit stimmst du unseren allgemeinen Geschäftsbedingungen zu."
                onValueChange={val => this.onChangeField("acceptTermsOfService", val)}/>
        <Paragraph>Hiermit stimmst du unseren allgemeinen Geschäftsbedingungen zu.</Paragraph>
      </View>;
    } else {
      loginWithAnotherAccount =
        <Button style={styles.button} mode="outlined" accessibilityLabel="Account wechseln"
                onPress={this.switchAccount.bind(this)}>Account wechseln</Button>;
      deleteAccountButton =
        <Button style={styles.button} accessibilityLabel="Account löschen" mode="outlined"
                onPress={this._showDeleteDialog}>Account löschen</Button>; //TODO red
    }

    return (<ScrollView keyboardShouldPersistTaps="handled" style={styles.container}>
      <ErrorBox errors={this.state.errors}></ErrorBox>
      <TextInput
        style={styles.formControl}
        label="Vorname"
        value={this.state.first_name}
        accessibilityLabel="Vorname"
        onChangeText={val => this.onChangeField("first_name", val)}
      />

      <TextInput
        style={styles.formControl}
        label="Benutzername"
        autoCapitalize="none"
        accessibilityLabel="Benutzername"
        value={this.state.username}
        onChangeText={val => this.onChangeField("username", val)}
      />

      <TextInput
        style={styles.formControl}
        label="E-Mail"
        keyboardType="email-address"
        autoCapitalize="none"
        accessibilityLabel="E-Mail"
        value={this.state.email}
        onChangeText={val => this.onChangeField("email", val)}
      />

      <TextInput
        style={styles.formControl}
        label="Neues Passwort"
        secureTextEntry={true}
        autoCapitalize="none"
        accessibilityLabel="Neues Passwort"
        value={this.state.password}
        onChangeText={val => this.onChangeField("password", val)}
      />
      {acceptTermsOfService}
      <Button style={styles.button} mode="contained" accessibilityLabel={this.props.submitTitle}
              onPress={this.onSubmit.bind(this)}>{this.props.submitTitle}</Button>

      {loginWithAnotherAccount}
      {deleteAccountButton}

      <Portal>
        <Dialog
          visible={this.state.deleteDialogVisible}
          onDismiss={this._hideDeleteDialog}>
          <Dialog.Title>Account löschen?</Dialog.Title>
          <Dialog.Content>
            <Paragraph>Deine persönlichen Daten auf unserem Server werden gelöscht.</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={this._hideDeleteDialog}>Nein</Button>
            <Button accessibilityLabel="Account löschen bestätigen" onPress={async () => {
              this.deleteAccount(this).then();
              this._hideDeleteDialog();
            }}>Ja</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </ScrollView>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  row: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  formControl: {
    marginVertical: 10,
  },
  button: {
    marginVertical: 8
  }
});

const mapStateToProps = state => {
  return {account: state.account.account};
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateAccount,
  initAuth,
  deleteAccount,
  showToast
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AccountForm);
