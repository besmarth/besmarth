import React from "react";
import {notNull} from "../services/utils";
import {Image, StyleSheet, Text, View} from "react-native";
import {fetchLevel} from "../services/Topics";
import {ActivityIndicator} from "react-native-paper";
import * as Progress from 'react-native-progress';

export default class TopicLevel extends React.Component {

  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  async componentDidMount() {
    this.reload();
  }

  async reload() {
    this.setState({loading: true, error: false});
    try {
      const level = await fetchLevel(this.props.topicId);
      this.setState({
        ...this.state,
        loading: false,
        level,
      });
    } catch (e)  {
      this.setState({
        ...this.state,
        loading: false,
        error: "Level konnte nicht geladen werden (" + JSON.stringify(e) + ")",
      });
    }
  }

  render() {
    if (this.state.error) {
      return (
        <View testID="view" style={styles.container}>
          <Text>{this.state.error}</Text>
        </View>
      );
    }
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator animating={true} size={100}/>
        </View>
      );
    }

    const level = this.state.level;

    const image = notNull(level.imageUrl) ? {uri: level.imageUrl} : require("../assets/images/no-challenge-image.png");

    let progressBar;

    if (this.props.showProgressBar && level.score !== -1) {
      const levelLength = level.scoreNextLevel - level.scorePrevLevel;
      const progress = (level.score - level.scorePrevLevel) / levelLength;

      progressBar = <Progress.Bar style={styles.progressBar} progress={progress} width={240} height={30} borderRadius={15} color={"rgba(0, 107, 42, 1)"} />
    }

    const levelText = "Level " + level.level + " - " + level.levelDescription;

    return <View testID="view" style={styles.container}>
      <Image source={image} style={{height: Number(this.props.size), width: Number(this.props.size)}}/>
      <Text style={[styles.levelDescription, {color: this.props.textColor}]}>{levelText}</Text>
      {progressBar}
    </View>
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  levelDescription: {
    fontSize: 22,
    marginTop: 10,
    marginBottom: 10,
  },
  progressBar: {
    marginBottom: 20,
  },
});