import {LinearGradient} from "expo-linear-gradient";
import * as React from "react";
import {StyleSheet, View} from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});

/**
 * Wraps its child inside a linear gradient background
 */
export function ScreenWrapper(props) {
  return <LinearGradient style={styles.container} colors={["#F1E54C", "#006B2A"]}>
    <View style={styles.container}>{props.children}</View>
  </LinearGradient>;
}