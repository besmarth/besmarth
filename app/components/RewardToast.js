import React from "react";
import {Modal, Portal} from "react-native-paper";
import {ScrollView, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import RewardToastService from "../services/RewardToastService";
import {RewardItem} from "./RewardItem";


class RewardToast extends React.Component {

  _hideModal = () => {
    RewardToastService.hideReward();
  };

  render() {
    const rewards = this.props.rewards;
    const visible = this.props.rewardsVisible;

    const rewardItems = (rewards || []).map((reward, key) => {
      return (
        <RewardItem reward={reward} key={key} />
      );
    });

    return (
      <Portal>
        <Modal visible={visible} onDismiss={this._hideModal}>
          <View styles={styles.rewardView}>
            <ScrollView>
              {rewardItems}
            </ScrollView>
          </View>
        </Modal>
      </Portal>
    );
  }
}

const styles = StyleSheet.create({
  rewardView: {
    flex: 1,
    alignContent: "center"
  }
});

const mapStateToProps = state => {
  return {
    rewardsVisible: state.app.rewardsVisible,
    rewards: state.app.rewards
  };
};


const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RewardToast);



