import {renderForSnapshot} from "../../../test/test-utils";
import React from "react";
import {ChallengeInfo} from "../ChallengeInfo";

describe("ChallengeInfo", () => {
  it(`renders`, () => {
    const tree = renderForSnapshot(<ChallengeInfo challenge={{
      id: 134515,
      title: "Challenge Title",
      image: "http://test.local/image/url",
      color: "#abcdef",
      periodicity: "daily",
      duration: 100,
      difficulty: "EASY"
    }}/>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`renders weekly advanced`, () => {
    const tree = renderForSnapshot(<ChallengeInfo challenge={{
      id: 134515,
      title: "Challenge Title 14124",
      image: "http://test.local/image/url",
      color: "#abcdef",
      periodicity: "weekly",
      duration: 10,
      difficulty: "ADVANCED"
    }}/>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`renders monthly hard`, () => {
    const tree = renderForSnapshot(<ChallengeInfo challenge={{
      id: 134515,
      title: "Challenge Title",
      image: "http://test.local/image/url",
      color: "#abcdef",
      periodicity: "monthly",
      duration: 200,
      difficulty: "HARD"
    }}/>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`renders unkown periodicity`, () => {
    const tree = renderForSnapshot(<ChallengeInfo challenge={{
      id: 134515,
      title: "Challenge Title",
      image: "http://test.local/image/url",
      color: "#abcdef",
      periodicity: "afafasfs",
      duration: 100
    }}/>).toJSON();
    expect(tree).toMatchSnapshot();
  });
});