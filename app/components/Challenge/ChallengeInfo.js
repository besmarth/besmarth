import {Text, StyleSheet, View} from "react-native";
import React from "react";
import { Icon } from 'react-native-elements'


/**
 * Displays information about a challenge.
 * Is not connected with the redux store
 * Use like <ChallengeInfo challenge={yourChallenge} />
 */
export class ChallengeInfo extends React.Component {

  computeDifficultyStyle(challenge) {
 switch (challenge.difficulty) {
      case "ADVANCED":
        return "Mittel";
        break;
      case "HARD":
        return "Schwer";
        break;
      case "EASY":
      default:
        return "Einfach";
        break;
    }
  }

  periodicityToString(periodicity) {
    switch (periodicity.toLowerCase()) {
      case "daily":
        return "Täglich";
      case "weekly":
        return "Wöchentlich";
      case "monthly":
        return "Monatlich";
      default:
        return "<Unknown periodicity " + periodicity;
    }
  }

  periodicityToUnitString(periodicity) {
    switch (periodicity.toLowerCase()) {
      case "daily":
        return "Tage";
      case "weekly":
        return "Wochen";
      case "monthly":
        return "Monate";
      default:
        return "<Unknown periodicity " + periodicity;
    }
  }

  render() {
    const challenge = this.props.challenge;

    return <View>
      <View style={styles.descriptionContainer}>
        <Text style={{fontSize: 15, color: 'gray'}}>
          {challenge.description}
        </Text>
      </View>
      <View style={styles.iconAndText}>
        <Icon name='event-note' />
        <Text style={{marginLeft:20, fontSize: 15}}>{challenge.duration + " " + this.periodicityToUnitString(challenge.periodicity)}</Text>
      </View>
            <View style={styles.iconAndText}>
        <Icon name='replay' />
        <Text style={{marginLeft:20, fontSize: 15}}>{this.periodicityToString(challenge.periodicity)}</Text>
      </View>
            <View style={styles.iconAndText}>
        <Icon name='school' />
        <Text style={{marginLeft:20, fontSize: 15 }}>{this.computeDifficultyStyle(challenge)}</Text>
      </View>
    </View>;
  }
}

const styles = StyleSheet.create({

  contentWrapper: {
    paddingHorizontal: 8,
    paddingVertical: 4
  },
  challengeImage: {
    height: 150,
    width: "100%",
  },
  card: {
    marginVertical: 4,
    marginHorizontal: 8,
  },
  iconAndText: {
    flexDirection: 'row',
    margin: 10,
  },
  descriptionContainer: {
    padding: 10,
  }
});
