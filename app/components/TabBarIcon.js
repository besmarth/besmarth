import React from "react";
import {Ionicons} from "@expo/vector-icons";
import theme from "../themes/theme";

export default function TabBarIcon(props) {
  return (
    <Ionicons
      name={props.name}
      size={26}
      style={{marginBottom: -3}}
      color={props.focused ? theme.colors.primary : '#ccc'}
    />
  );
}
