import React, {useEffect, useState} from 'react';
import {Image, Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import * as ImagePicker from 'expo-image-picker';

export default function ImagePickerExample() {
    const [image, setImage] = useState(null);


    useEffect(() => {
        (async () => {
            if (Platform.OS !== 'web') {
                const {status} = await ImagePicker.requestCameraRollPermissionsAsync();
                if (status !== 'granted') {
                    alert('Sorry, we need camera roll permissions to make this work!');
                }
            }
        })();
    }, []);

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        //console.log(result.uri);

        if (!result.cancelled) {
            setImage(result.uri);
            console.log(result.uri)
        }
    };


    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            {image && <Image source={{uri: image}} style={styles.imagePreview}/>}
            <TouchableOpacity style={styles.importButton}
                              onPress={pickImage}
            >
                <Text>Import Image</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    importButton: {
        backgroundColor: '#7b9c93',
        height: 50,
        width: 150,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imagePreview: {
        borderRadius: 50,
        width: 100,
        height: 100,
        margin: 5
    }
});